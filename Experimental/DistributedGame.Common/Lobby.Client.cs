﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Extensions;

namespace DistributedGame.Common
{
	partial class Lobby
	{
		private class LobbyClient : ILobbyClientTicket
		{
			internal volatile Lobby currentLobby;

			public LobbyClient(Lobby lobby, string id, string initialNickname)
			{
				this.currentLobby = lobby;
				this.Id = id;
				this.Nickname = initialNickname;
			}

			ILobby ILobbyClientTicket.Lobby
			{
				get { return currentLobby; }
			}

			public string Id
			{
				get; private set;
			}

			public string Nickname
			{
				get; private set;
			}

			public event EventHandler<EvArg<IChatMessage>> MessageReceived;
			public event EventHandler<EvArg<DropReason>> Dropped;

			void ILobbyClientTicket.Exit()
			{
				var lobby = this.currentLobby;
				if (lobby == null) return;

				InvalidatedTicket();

				lobby.ClientExit(this);
			}

			void ILobbyClientTicket.Say(string textMessage)
			{
				var lobby = this.currentLobby;
				if (lobby == null) return;

				lobby.ClientSay(this, textMessage);
			}

			internal void ReceiveMessage(IChatMessage message)
			{
				OnMessageReceived(message);
			}

			internal void Drop(DropReason reason)
			{
				InvalidatedTicket();

				OnDropped(reason);
			}

			private void InvalidatedTicket()
			{
				currentLobby = null;
			}

			private void OnMessageReceived(IChatMessage message)
			{
				MessageReceived.RaiseEvent(this, new EvArg<IChatMessage>(message));
			}

			private void OnDropped(DropReason reason)
			{
				Dropped.RaiseEvent(this, new EvArg<DropReason>(reason));
			}
		}
	}
}
