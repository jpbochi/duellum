﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ComponentModel;

namespace DistributedGame.Common
{
	[DataContract]
	public class LobbyClientViewData : ILobbyClientView
	{
		[DataMember]
		public string Id { get; private set; }

		[DataMember]
		public string Nickname { get; private set; }

		public LobbyClientViewData(ILobbyClientView view)
		{
			this.Id = view.Id;
			this.Nickname = view.Nickname;
		}
	}

	[DataContract]
	public class ChatMessageData : IChatMessage
	{
		[DataMember]
		public string Content { get; private set; }

		[DataMember]
		public LobbyClientViewData Sender { get; private set; }

		[DataMember]
		public DateTime Time { get; private set; }

		ILobbyClientView IChatMessage.Sender { get { return this.Sender; } }

		public ChatMessageData(string content, ILobbyClientView sender)
		{
			this.Content = content;
			this.Sender = sender.ToDataTransferObj();
			this.Time = DateTime.Now;
		}

		public ChatMessageData(IChatMessage message)
		{
			this.Content = message.Content;
			this.Sender = message.Sender.ToDataTransferObj();
			this.Time = message.Time;
		}
	}

	[DataContract]
	public struct DropReason
	{
		public static readonly DropReason Closing = new DropReason("Closing");
		public static readonly DropReason Kicked = new DropReason("Kicked");

		[DataMember]
		public string Description { get; private set; }

		private DropReason(string description) : this()
		{
			this.Description = description;
		}
	}

	[DataContract]
	public class HostedGameData
	{
		[DataMember]
		public string MapName { get; private set; }
	}
}
