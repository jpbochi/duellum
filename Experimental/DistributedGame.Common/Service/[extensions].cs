﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace DistributedGame.Common
{
	public static class DataTransferObjsExt
	{
		public static LobbyClientViewData ToDataTransferObj(this ILobbyClientView view)
		{
			if (view is LobbyClientViewData) return (LobbyClientViewData)view;

			return new LobbyClientViewData(view);
		}

		public static IEnumerable<LobbyClientViewData> ToDataTransferObj(this IEnumerable<ILobbyClientView> source)
		{
			return source.Select( item => item.ToDataTransferObj() );
		}

		public static ChatMessageData ToDataTransferObj(this IChatMessage message)
		{
			if (message is ChatMessageData) return (ChatMessageData)message;

			return new ChatMessageData(message);
		}
	}

	public static class OperationContextExt
	{
		private class RunningCompletedExtension : IExtension<OperationContext>, IDisposable
		{
			OperationContext context;

			private RunningCompletedExtension(OperationContext context)
			{
				this.context = context;
			}

			public static RunningCompletedExtension Create(OperationContext context)
			{
				if (context == null) throw new ArgumentNullException("context");

				var ext = new RunningCompletedExtension(context);
				context.Extensions.Add(ext);
				return ext;
			}

			void IExtension<OperationContext>.Attach(OperationContext owner)
			{}

			void IExtension<OperationContext>.Detach(OperationContext owner)
			{}

			public void Dispose()
			{
				context.Extensions.Add(this);
			}
		}

		public static void InvokeOnCompleted(this OperationContext context, Action action)
		{
			if (context == null) {
				//TODO: Run this asynchronously! Handle exceptions where?

				//This usually happens when OperationContext has already completed

				action();
			} else if (context.Extensions.Find<RunningCompletedExtension>() != null) {
				action();
			} else {
				context.OperationCompleted += delegate {
					using (var ext = RunningCompletedExtension.Create(context))
					{
						action();
					}
				};
			}
		}
	}
}
