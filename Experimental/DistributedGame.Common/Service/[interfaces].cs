﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace DistributedGame.Common.Service
{
	//http://www.codeproject.com/KB/WCF/WCFConcurrency.aspx
	//http://blogs.msdn.com/youssefm/archive/2009/04/21/understanding-known-types.aspx

    [ServiceContract(CallbackContract = typeof(ILobbyClientCallback), SessionMode = SessionMode.Required)]
    public interface ILobbyService
    {
		[OperationContract(IsInitiating = true)]
		//[FaultContract(typeof(LobbyFault))]
		LobbyClientViewData Enter(string desiredNickname);

		[OperationContract()]
		ICollection<LobbyClientViewData> GetClientList();

		[OperationContract(IsOneWay = true)]
		void Say(string textMessage);

		//[OperationContract(IsOneWay = true)]
		//void TryRename(string desiredNickname);

		[OperationContract(IsTerminating = true, IsOneWay = true)]
		void Disconnect();

		[OperationContract()]
		string EnterGame();
    }

	//[ServiceKnownType(typeof(ChatMessageData))]
	//[ServiceKnownType(typeof(LobbyClientViewData))]

    public interface ILobbyClientCallback
    {
		[OperationContract(IsOneWay = true)]
		void ClientEntered(LobbyClientViewData client);

		[OperationContract(IsOneWay = true)]
		void ClientLeft(LobbyClientViewData client);

		[OperationContract(IsOneWay = true)]
		void Receive(ChatMessageData message);

		//[OperationContract(IsOneWay = true)]
		//TODO: Would be used for nickname changes, for example
		//void ClientUpdated(LobbyClientViewData client, LobbyClientViewData newData);

		[OperationContract(IsTerminating = true, IsOneWay = true)]
		void Dropped(DropReason reason);
    }

    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface IHostedGameService
    {
		[OperationContract()]
		//[FaultContract(typeof(LobbyFault))]
		string Enter(string slotId, string desiredNickname);
	}
}
