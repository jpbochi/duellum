﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Extensions;
using System.Runtime.Serialization;

namespace DistributedGame.Common
{
	public partial class Lobby : ILobby, IDisposable
	{
	    //TODO: use a ConcurrentDictionary and get rid of locks
	    //	- problem: I'm using two keys: SessionId (as a PK) and Nickname (as an UK)
	    //	- solution 1: two dicts (one for each key)
	    //	- solution 2: use locks for updates only (?!)

	    private object syncRoot = new object();

		//private Lazy<IDictionary<string,LobbyClient>>
		//    lazyClients = new Lazy<IDictionary<string,LobbyClient>>(
		//        () => new Dictionary<string,LobbyClient>()
		//    );
		private IDictionary<string,LobbyClient> notLazyClients = new Dictionary<string,LobbyClient>();

		private bool isClosed;

	    public Lobby()
	    {}

	    private IDictionary<string,LobbyClient> ClientDict
	    {
	        get { return notLazyClients; }

	    }

	    private ICollection<LobbyClient> CurrentClients
	    {
	        get {
	            lock (syncRoot) return this.ClientDict.Values.ToReadOnlyColl();
	        }
	    }

	    ICollection<ILobbyClientView> ILobby.Clients
	    {
	        get {
	            return this.CurrentClients.Cast<ILobbyClientView>().ToReadOnlyColl();
	        }
	    }

		public event EventHandler<EvArg<ILobbyClientView>> ClientEntered;
		public event EventHandler<EvArg<ILobbyClientView>> ClientLeft;
		public event EventHandler LobbyClosed; //TODO: not in use

		public ILobbyClientTicket Enroll(string desiredNickname)
		{
			if (string.IsNullOrEmpty(desiredNickname)) throw new ArgumentException("desiredNickname");

	        string nickname = desiredNickname;

			LobbyClient newClient;
			
	        lock (syncRoot)
	        {
				if (isClosed) throw new InvalidOperationException("Lobby is closed");

	            //Rename player name to avoid name conflicts
	            var currentNames = this.CurrentClients.Select( c => c.Nickname );

	            int attempt = 0;
	            while (currentNames.Contains(nickname)) nickname = string.Format("{0}[{1}]", desiredNickname, ++attempt);
				
	            string id = Guid.NewGuid().ToString();
	            newClient = new LobbyClient(this, id, nickname);
				
	            this.ClientDict.Add(id, newClient);
	        }

			OnClientEntered(newClient);

	        return newClient;
	    }

		public void Close()
		{
			var clients = this.CurrentClients;

			lock (syncRoot)
			{
				isClosed = true;
				this.ClientDict.Clear();
			}

			BroadcastToClients(clients, c => c.Drop(DropReason.Closing));

			OnLobbyClosed();
		}

		void IDisposable.Dispose()
		{
			Close();
		}

		private void OnClientEntered(ILobbyClientView client)
		{
			ClientEntered.RaiseEvent(this, new EvArg<ILobbyClientView>(client));
		}

		private void OnClientLeft(ILobbyClientView client)
		{
			ClientLeft.RaiseEvent(this, new EvArg<ILobbyClientView>(client));
		}

		private void OnLobbyClosed()
		{
			LobbyClosed.RaiseEvent(this, EventArgs.Empty);
		}

		private void ClientSay(LobbyClient client, string textMessage)
		{
			var msg = new ChatMessageData(textMessage, client);

			BroadcastToClients( c => c.ReceiveMessage(msg) );
		}

		private void ClientExit(LobbyClient client)
		{
			bool wasClientRemoved;
			lock (syncRoot) wasClientRemoved = this.ClientDict.Remove(client.Id);

			if (wasClientRemoved) OnClientLeft(client);
		}

		private void BroadcastToClients(Action<LobbyClient> action)
		{
			var clients = this.CurrentClients;

			BroadcastToClients(clients, action);
		}

		private void BroadcastToClients(ICollection<LobbyClient> clients, Action<LobbyClient> action)
		{
			//TODO: Parallel.ForEach(clients, action); //WCF deadlock happened here last time I tried to do a parallel broadcast
			foreach (var c in clients) action(c);
		}
	}
}
