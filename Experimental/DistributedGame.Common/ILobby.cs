﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace DistributedGame.Common
{
	public class EvArg<T> : EventArgs
	{
	    public T Value { get; private set; }

	    public EvArg(T value)
	    {
	        this.Value = value;
	    }
	}

	public enum LobbyClientDropReason
	{
		LobbyClosing,
		Kicked,
		Banned
	}

	public interface IChatMessage
	{
		string Content { get; }
		ILobbyClientView Sender { get; }
		DateTime Time { get; }
	}

	public interface ILobbyClientView
	{
		string Id { get; }
		string Nickname { get; }
	}

	public interface ILobby
	{
		ICollection<ILobbyClientView> Clients { get; }

		event EventHandler<EvArg<ILobbyClientView>> ClientEntered;
		event EventHandler<EvArg<ILobbyClientView>> ClientLeft;
		event EventHandler LobbyClosed;

		//ILobbyClientTicket GetAdminTicket();
		ILobbyClientTicket Enroll(string desiredNickname);

		void Close();
	}

	public interface ILobbyClientTicket : ILobbyClientView
	{
		ILobby Lobby { get; }
		//ILobbyClient Client { get; }

		event EventHandler<EvArg<DropReason>> Dropped;
		event EventHandler<EvArg<IChatMessage>> MessageReceived;

		void Exit();

		void Say(string textMessage); //TODO: Private and Team messages

		//event EventHandler NicknameChanged;
		//void TryRename(string desiredNickname);

		//object Do(string commandText);
		/// Possible commands:
		/// - IsWriting
		/// - Kick/Ban (admin only);
		/// - Ignore/Block;
		/// - Change status (online, away, invisible, etc.);

		///TODO: Parse messages
		///www.ircbeginner.com/ircinfo/m-commands.htm
		///www.ircbeginner.com/ircinfo/ircc-commands.html
	}
}
