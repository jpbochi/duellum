﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributedGame.Common
{
	/// http://en.wikipedia.org/wiki/Category:Abstract_strategy_games
	/// http://en.wikipedia.org/wiki/Board_game
	

	public interface IAbstractGameParameters
	{
		ICollection<IAbstractGameSlot> Slots { get; } //Types: vacant, occupied, reserved

		//bool CanStartGame();
		//IAbstractGame CreateGame();

		/// Game Initialization Sequence:
		/// 1. Order players (ramdomly, per team, or whatever other option);
		/// 2. Create board (according to template and/or options);
		/// 3. Set up players (according to template and/or options);
		/// 4. Lay player's pieces;
		/// 5. Begin turn based loop;
		/// 
		/// Turn-Based Loop:
		/// 1. Initialize turn;
		/// 2. Concede control to player;
		/// 3. Run player actions until turn is due;
		/// 4. Finalize turn;
		/// 5. If game is not over, start another turn;
		/// 
		/// Game Finalization Sequence:
		/// 1. Expose results (for displaying and logging);


	}

	public interface IAbstractGameSlot
	{
		
		//bool IsVacant();

		//Player properties: Name, Power, Color, Team, Controller(Human/AI, Local/Remote), 
	}

	public interface IAbstractGame
	{
		//States: Created, Initialized, [WaitingForPlayer], Playing, GameOver

		IEnumerable<IPlayer> Players { get; }
		IEnumerable<IGameHandle> GetPlayerHandles();

		event EventHandler NewTurn;
		event EventHandler GameOver;
	}

	public interface IPlayer
	{
	}

	public interface IGameHandle
	{
		IAbstractGame Game { get; }
		IPlayer Player { get; }
		
		IEnumerable<IGameCommand> GetCommands();
	}

	public interface IGameCommand
	{
		//IGameCommand WithParam(IGameCommandParam param);

		void Execute();
	}
}
