﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DistributedGame.Common;
using JpLabs.Extensions;

namespace DistributedGame.TicTacToe
{
	public class TicTacToeGame : IAbstractGame
	{
		private object syncRoot = new object();

		private readonly int?[,] board;
		private readonly IList<Player> players;
		private readonly ICollection<GameHandle> handles;

		private int? currentPlayer;

		public TicTacToeGame()
		{
			board = new int?[3,3];

			players = new []{ new Player(0), new Player(1) };
			handles = players.Select(p => new GameHandle(this, p)).ToReadOnlyColl();

			currentPlayer = 0;
		}

		public IEnumerable<IPlayer> Players { get { return this.players; } }
		public IEnumerable<IGameHandle> GetPlayerHandles()
		{
			return this.handles;
		}

		public int?[,] Board { get { return board; } }
		public int? Turn { get { return currentPlayer; } }

		public event EventHandler NewTurn;
		public event EventHandler GameOver;

		private bool IsPlayerIsCurrent(Player player)
		{
			return (player.Number == currentPlayer);
		}

		private void AssertPlayerIsCurrent(Player player)
		{
			if (player.Number != currentPlayer) throw new InvalidOperationException();
		}

		private void Move(Player player, Tuple<int,int> pos)
		{
			AssertPlayerIsCurrent(player);

			//TODO: deny any other moves until this move is completed

			board[pos.Item1, pos.Item2] = player.Number;
			
			int? winner = Winner();
			if (winner != null) {
				currentPlayer = -1;
				OnFinished();
			} else {
				currentPlayer = (player.Number + 1) % 2;
				OnNewTurn();
			}
		}

		private int? Value(Tuple<int,int> pos)
		{
			return board[pos.Item1, pos.Item2];
		}

		private int? Winner()
		{
			if (IsWinner(0)) return 0;
			if (IsWinner(1)) return 1;
			return null;
		}

		private bool IsWinner(int playerNo)
		{
			var winningLines =
				Enumerable.Range(0, 3)
				.Select( r => Enumerable.Range(0, 3).Select( c => Tuple.Create(r, c) ) )
				.Concat(
					Enumerable.Range(0, 3)
					.Select( c => Enumerable.Range(0, 3).Select( r => Tuple.Create(r, c) ) )
				)
				.Concat(
					Enumerable.Range(0, 3)
					.Select( d => Tuple.Create(d, d) )
				)
				.Concat(
					Enumerable.Range(0, 3)
					.Select( d => Tuple.Create(d, 2-d) )
				);

			return winningLines.Any( line => line.All( p => Value(p) == playerNo ) );
		}

		private void OnNewTurn()
		{
			var ev = NewTurn;
			if (ev != null) ev(this, EventArgs.Empty);
		}

		private void OnFinished()
		{
			var ev = GameOver;
			if (ev != null) ev(this, EventArgs.Empty);
		}

		class Player : IPlayer
		{
			public int Number { get; private set; }

			public Player(int number)
			{
				Number = number;
			}
		}

		class GameHandle : IGameHandle
		{
			public GameHandle(TicTacToeGame game, Player player)
			{
				this.Game = game;
				this.Player = player;
			}

			public TicTacToeGame Game { get; private set; }
			public Player Player { get; private set; }

			IAbstractGame IGameHandle.Game { get { return Game; } }
			IPlayer IGameHandle.Player { get { return Player; } }

			public IEnumerable<IGameCommand> GetCommands()
			{
				if (!Game.IsPlayerIsCurrent(Player)) return EmptyReadOnlyColl<IGameCommand>.Value;

				return
					from r in Enumerable.Range(0, 3)
					from c in Enumerable.Range(0, 3)
					let p = Tuple.Create(r, c)
					where Game.Value(p) == null
					select new Command(this, p);
			}

			internal void ExecuteCommand(Command command)
			{
				Game.Move(Player, command.Pos);
			}
		}

		class Command : IGameCommand
		{
			private readonly GameHandle turn;

			public Command(GameHandle turn, Tuple<int,int> pos)
			{
				this.turn = turn;
				this.Pos = pos;
			}

			public Tuple<int,int> Pos { get; private set; }

			public void Execute()
			{
				turn.ExecuteCommand(this);
			}

			public override string ToString()
			{
				return Pos.ToString();
			}
		}
	}
}
