﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Collections.ObjectModel;

namespace DistributedGame.Server
{
	internal class ErrorHandlerBehavior : IServiceBehavior
	{
		public IErrorHandler ErrorHandler { get; private set; }

		public ErrorHandlerBehavior(IErrorHandler errorHandler)
		{
			this.ErrorHandler = errorHandler;
		}

		void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
		{
		}

		void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			foreach (var dispatcher in serviceHostBase.ChannelDispatchers.OfType<ChannelDispatcher>()) {
				dispatcher.ErrorHandlers.Add(this.ErrorHandler);
			}
		}

		void IServiceBehavior.Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
		}
	}

	internal class ServerErrorHandler : IErrorHandler
	{
		public object StateObject { get; private set; }

		public ServerErrorHandler(object stateObject)
		{
			this.StateObject = stateObject;
		}

		public bool HandleError(Exception error)
		{
			System.Diagnostics.Debug.WriteLine("Error caught: " + error.ToString());

			//TODO: how the hell can I know which client faulted?! I can't!

			//var client = this.Server.CurrentClient;
			//System.Diagnostics.Debug.WriteLine("This client faulted: " + client.Info.Nick);

			return true;//false;
		}

		public void ProvideFault(Exception error, MessageVersion version, ref System.ServiceModel.Channels.Message fault)
		{
			FaultException faultException = new FaultException(error.Message);
			MessageFault messageFault = faultException.CreateMessageFault();
			fault = System.ServiceModel.Channels.Message.CreateMessage(version, messageFault, faultException.Action);
		}
	}
}
