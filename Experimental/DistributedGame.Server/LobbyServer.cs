﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DistributedGame.Common.Service;
using DistributedGame.Common;
using System.Threading;
using JpLabs.Extensions;

namespace DistributedGame.Server
{
	public class LobbyServer : IDisposable
	{
		private ServiceHost ServiceHost;

		internal readonly ILobby Lobby;

		internal readonly IDictionary<string,LobbyService> LobbyServicesDict;

		public LobbyServer(ILobby lobby)
		{
			this.Lobby = lobby;

			LobbyServicesDict = new Dictionary<string,LobbyService>();

			this.Lobby.ClientEntered += Lobby_ClientEntered;
			this.Lobby.ClientLeft += Lobby_ClientLeft;
		}

		public CommunicationState State
		{
			get {
				var commObj = ServiceHost;
				if (commObj == null) return CommunicationState.Created;
				return commObj.State;
			}
		}

		public event EventHandler<ThreadExceptionEventArgs> CommunicationExceptionHappened;

		public void StartServer(int tcpPort)
		{
			var state = this.State;
			if (state != CommunicationState.Created)
			if (state != CommunicationState.Closed) throw new InvalidOperationException("Server can't be started at this state");
			
			//baseAddress.Scheme = "net.tcp";
			//baseAddress.Host = "localhost";
			
            //Uri tcpAdrs = new Uri("net.tcp://wn7-dptppd1:2718/game/");
            //Uri httpAdrs = new Uri("http://wn7-dptppd1:8080/game/");
            
			//var lobbyService = new LobbyService(this.Lobby);
			//this.ServiceHost = new ServiceHost(lobbyService, baseAddress);

			//var netTcpAddr = new Uri(baseAddress.AbsoluteUri);
			//netTcpAddr.Scheme

			var baseAddress = new UriBuilder(Uri.UriSchemeNetTcp, "localhost", tcpPort, "lobby").Uri;

			this.ServiceHost = new ServiceHost(typeof(LobbyService), baseAddress);

			{
				var pipeBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
				{
					OpenTimeout = TimeSpan.FromSeconds(4),
					CloseTimeout = TimeSpan.FromSeconds(2),
					SendTimeout = TimeSpan.FromSeconds(4),
					ReceiveTimeout = TimeSpan.MaxValue,
				};
				ServiceHost.AddServiceEndpoint(typeof(ILobbyService), pipeBinding, "net.pipe://localhost/lobby/");
			}
			{
				NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.None, true)
				{
					OpenTimeout = TimeSpan.FromSeconds(4),
					CloseTimeout = TimeSpan.FromSeconds(2),
					SendTimeout = TimeSpan.FromSeconds(4),
					ReceiveTimeout = TimeSpan.MaxValue,
				};
				tcpBinding.ReliableSession.Enabled = true;            
				tcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(8);
				tcpBinding.ReliableSession.Ordered = true;

				ServiceHost.AddServiceEndpoint(typeof(ILobbyService), tcpBinding, string.Empty);
			}

			//ServiceHost.Extensions.Add(new LobbyServerExtension(this));
			//ServiceHost.Description.Behaviors.Add(new CustomInstanceInitializer(new LobbyServiceInitializer(this)));
			//ServiceHost.Description.Behaviors.Add(new CustomInstanceProvider(new LobbyServiceInstanceProvider(this)));

			ServiceHost.Description.Behaviors.Add(new ErrorHandlerBehavior(new ServerErrorHandler(this)));
			ServiceHost.Description.Behaviors.Add(new CustomInstanceProvider(
				new FactoryInstanceProvider(this.CreateService)
			));
			
			//The following two lines are explained here: https://connect.microsoft.com/VisualStudio/feedback/details/565224
			GC.Collect();
			GC.WaitForPendingFinalizers();

			ServiceHost.Open();

			//{
			//    var newServiceHost = new ServiceHost(typeof(HostedGameService), baseAddress);
			//    newServiceHost.AddServiceEndpoint(typeof(IHostedGameService), tcpBinding, "game");
			//    newServiceHost.Open();
			//}
		}

		void IDisposable.Dispose()
		{
			Close();
		}

		public void Close()
		{
			var serviceHost = this.ServiceHost;

			if (serviceHost != null) {
			    if (serviceHost.State == CommunicationState.Opened) {
			        try
			        {
						this.Lobby.Close(); //BroadcastNow( callback => callback.Dropped() );

						//var lobbyServices = LobbyServicesDict.Values.ToArray();
						//foreach (var service in lobbyServices) service.Dispose();

						LobbyServicesDict.Clear();

			            serviceHost.Close();
			        } catch (CommunicationException ex) {
			            OnCommunicationExceptionHappened(ex);
			        }
			    }

			    //if (serviceHost.State != CommunicationState.Faulted)
				((IDisposable)serviceHost).Dispose();
			}

			this.ServiceHost = null;
		}

		private LobbyService CreateService()
		{
			var context = OperationContext.Current;
			var sessionId = context.Channel.SessionId;

			LobbyService service;
			if (LobbyServicesDict.TryGetValue(sessionId, out service)) return service;

			service = new LobbyService(this.Lobby, context.GetCallbackChannel<ILobbyClientCallback>());
			LobbyServicesDict.Add(sessionId, service);

			return service;
		}

		private void OnCommunicationExceptionHappened(CommunicationException exception)
		{
			System.Diagnostics.Debug.Write(exception, "CommunicationException");

			this.CommunicationExceptionHappened.RaiseEvent(this, new ThreadExceptionEventArgs(exception));
		}

		private void Broadcast(Action<ILobbyClientCallback> action)
		{
			OperationContext.Current.InvokeOnCompleted( () => BroadcastNow(action) );
		}

		private void BroadcastNow(Action<ILobbyClientCallback> action)
		{
			var availableCallbacks
				= this.LobbyServicesDict
				.Values
				.Select( service => service.GetLobbyCallback() )
				.Where( callback => callback != null )
				.ToArray();

			//TODO: Research this: http://www.idesign.net/idesign/DesktopDefault.aspx?tabindex=5&tabid=11
			//TODO: remove faulted service instances
			//TODO: run broadcast asynchronously

			foreach (var callback in availableCallbacks) {
				try {
					action(callback);
				} catch (CommunicationException ex) {
					OnCommunicationExceptionHappened(ex);
				}
			}
		}

		void Lobby_ClientEntered(object sender, EvArg<ILobbyClientView> e)
		{
			Broadcast(
				lobbyCallback => lobbyCallback.ClientEntered(e.Value.ToDataTransferObj())
			);
		}

		void Lobby_ClientLeft(object sender, EvArg<ILobbyClientView> e)
		{
			Broadcast(
				lobbyCallback => lobbyCallback.ClientLeft(e.Value.ToDataTransferObj())
			);
		}
	}
}
