﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DistributedGame.Common;
using DistributedGame.Common.Service;
using System.Runtime.Serialization;
using JpLabs.Extensions;

namespace DistributedGame.Server
{
	[ServiceBehavior(
		#if DEBUG
			IncludeExceptionDetailInFaults = true,
		#endif
		InstanceContextMode=InstanceContextMode.PerSession,
		ConcurrencyMode=ConcurrencyMode.Reentrant,
		UseSynchronizationContext = false
	)]
	internal class LobbyService : ILobbyService, IDisposable
	{
		private readonly object syncRoot = new object();

		private readonly ILobby lobby;
		private readonly ILobbyClientCallback lobbyCallback;

		private volatile ILobbyClientTicket currentTicket;

		public LobbyService(ILobby lobby, ILobbyClientCallback callback)
		{
			if (lobby == null) throw new ArgumentNullException("lobby");
			if (callback == null) throw new ArgumentNullException("callback");

			this.lobby = lobby;
			this.lobbyCallback = callback;
		}

		public ILobbyClientCallback GetLobbyCallback()
		{	
			var callback = this.lobbyCallback;
			var ticket = this.currentTicket;

			if (((ICommunicationObject)callback).State != CommunicationState.Opened) return null;
			if (ticket == null) return null;

			return callback;
		}

		private void AttachTicket(ILobbyClientTicket ticket)
		{
			if (ticket == null) return;
			ticket.MessageReceived += Ticket_MessageReceived;
			ticket.Dropped += Ticket_Dropped;
		}

		private void DettachTicket(ILobbyClientTicket ticket)
		{
			if (ticket == null) return;
			ticket.MessageReceived -= Ticket_MessageReceived;
			ticket.Dropped -= Ticket_Dropped;
		}


		void Ticket_MessageReceived(object sender, EvArg<IChatMessage> e)
		{
			OperationContext.Current.InvokeOnCompleted( delegate {
				this.lobbyCallback.Receive(e.Value.ToDataTransferObj());
			});
		}

		void Ticket_Dropped(object sender, EvArg<DropReason> e)
		{
			OperationContext.Current.InvokeOnCompleted( delegate {
				this.lobbyCallback.Dropped(e.Value);
			});
		}

		LobbyClientViewData ILobbyService.Enter(string desiredNickname)
		{
			ILobbyClientTicket oldTicket, newTicket;
			lock (syncRoot)
			{
				if (this.currentTicket != null) throw new FaultException("Client is already connected");

				oldTicket = this.currentTicket;
				this.currentTicket = this.lobby.Enroll(desiredNickname);
				newTicket = this.currentTicket;
			}
			
			DettachTicket(oldTicket);
			AttachTicket(newTicket);

			return this.currentTicket.ToDataTransferObj();
		}

		ICollection<LobbyClientViewData> ILobbyService.GetClientList()
		{
			return lobby.Clients.ToDataTransferObj().ToReadOnlyColl();
		}

		void ILobbyService.Say(string textMessage)
		{
			var ticket = ValidatedAndGetTicket();

			ticket.Say(textMessage);
		}

		//void ILobbyService.TryRename(string desiredNickname)
		//{
		//    var ticket = ValidatedAndGetTicket();
		//    ticket.TryRename(desiredNickname);
		//}

		void ILobbyService.Disconnect()
		{
			ILobbyClientTicket previousTicket;
			lock (syncRoot)
			{
				previousTicket = this.currentTicket;
				this.currentTicket = null;
			}
			if (previousTicket != null && previousTicket.Lobby != null) previousTicket.Exit();
		}

		public void Dispose()
		{
			((ILobbyService)this).Disconnect();
		}

		private ILobbyClientTicket ValidatedAndGetTicket()
		{
			var ticket = this.currentTicket;
			if (ticket == null) throw new InvalidOperationException("Client is not connected");
			return ticket;
		}

		string ILobbyService.EnterGame()
		{
			//OperationContext.Current.Channel
			throw new NotImplementedException();
		}
	}

	[ServiceBehavior(
		#if DEBUG
			IncludeExceptionDetailInFaults = true,
		#endif
		InstanceContextMode=InstanceContextMode.PerSession,
		ConcurrencyMode=ConcurrencyMode.Reentrant,
		UseSynchronizationContext = false
	)]
	internal class HostedGameService : IHostedGameService
    {
		string IHostedGameService.Enter(string slotId, string desiredNickname)
		{
			throw new NotImplementedException();
		}
	}
}
