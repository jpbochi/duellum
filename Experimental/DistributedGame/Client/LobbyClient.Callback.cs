﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using JpLabs.Extensions;
using DistributedGame.Common.Service;
using DistributedGame.Common;

namespace DistributedGame.Client
{
    partial class LobbyClient
    {
		[CallbackBehavior(
			ConcurrencyMode=ConcurrencyMode.Reentrant,
			UseSynchronizationContext = false
		)]
		class Callback : ILobbyClientCallback
		{
			private readonly LobbyClient localClient;

			internal Callback(LobbyClient client)
			{
				this.localClient = client;
			}

			void ILobbyClientCallback.ClientEntered(LobbyClientViewData client)
			{
				this.localClient.OnClientEntered(client);
			}

			void ILobbyClientCallback.ClientLeft(LobbyClientViewData client)
			{
				this.localClient.OnClientLeft(client);
			}

			void ILobbyClientCallback.Receive(ChatMessageData message)
			{
				this.localClient.OnMessageReceive(message);
			}

			//[OperationBehavior(ReleaseInstanceMode=ReleaseInstanceMode.AfterCall)]
			void ILobbyClientCallback.Dropped(DropReason reason)
			{
				OperationContext.Current.InvokeOnCompleted( () => {
					//WCF doesn't closes the channel on the client side for some reason (maybe it would enable the client to reconnect?)
					this.localClient.DisconnectChannel();

					this.localClient.Disconnect();

					this.localClient.OnDropped(reason);
				} );
			}
		}

		class DisconnectedLobbyService : ILobbyService
		{
			public static ILobbyService Instance = new DisconnectedLobbyService();
			
			private DisconnectedLobbyService() {}

			private Exception DefaulException()
			{
				return new InvalidOperationException("Service is disconnected");
			}

			LobbyClientViewData ILobbyService.Enter(string desiredNickname)
			{
				throw DefaulException();
			}

			ICollection<LobbyClientViewData> ILobbyService.GetClientList()
			{
				return new LobbyClientViewData[0];
			}

			void ILobbyService.Say(string textMessage)
			{}

			void ILobbyService.Disconnect()
			{}

			string ILobbyService.EnterGame()
			{
				throw DefaulException();
			}
		}
	}
}
