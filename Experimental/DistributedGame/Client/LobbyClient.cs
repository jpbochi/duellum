﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using JpLabs.Extensions;
using DistributedGame.Common.Service;
using DistributedGame.Common;
using System.Threading;

namespace DistributedGame.Client
{
    public partial class LobbyClient : IDisposable
    {
		/*
		class MyClient : ClientBase<ILobbyService>
		{
			public MyClient(InstanceContext callbackInstance, Binding binding, EndpointAddress remoteAddres)
			: base(callbackInstance, binding, remoteAddres)
			{
			}

			public ILobbyService Service
			{
				get { return this.Channel; }
			}
		}
		//*/

		private readonly Callback ClientCallback;

		public ILobbyService LobbyService { get; private set; }

		public LobbyClient()
		{
			this.ClientCallback = new Callback(this);
			this.LobbyService = DisconnectedLobbyService.Instance;
		}

		private IClientChannel LobbyServiceChannel
		{
			get { return this.LobbyService as IClientChannel; } //IClientChannel or IDuplexContextChannel
		}
		
		public CommunicationState State
		{
			get {
				var channel = LobbyService as IClientChannel;//LobbyServiceChannel;
				if (channel == null) return CommunicationState.Created;
				return channel.State;
			}
		}

		public event EventHandler<EvArg<ILobbyClientView>> ClientEntered;
		public event EventHandler<EvArg<ILobbyClientView>> ClientLeft;
		public event EventHandler<EvArg<IChatMessage>> MessageReceived;
		public event EventHandler<EvArg<DropReason>> Dropped;

		public event EventHandler<ValueChangedEventArgs<CommunicationState>> StateChanged;
		public event EventHandler<ThreadExceptionEventArgs> CommunicationExceptionHappened;

		public ILobbyClientView Enter(Uri host, string desiredNickname)
		{
			if (!host.IsAbsoluteUri) throw new ArgumentException("Invalid URI");

			Binding binding;

			if (host.Scheme == Uri.UriSchemeNetPipe) {
				binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None); //net.pipe://localhost/lobby/
			} else if (host.Scheme == Uri.UriSchemeNetTcp) {
				var tcpBinding = new NetTcpBinding(SecurityMode.None, true) {
			
					OpenTimeout = TimeSpan.FromSeconds(9),
					CloseTimeout = TimeSpan.FromSeconds(3),
					SendTimeout = TimeSpan.FromSeconds(6000),
					ReceiveTimeout = TimeSpan.MaxValue,
				};

				tcpBinding.ReliableSession.Enabled = true;
				tcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(8);
				tcpBinding.ReliableSession.Ordered = true;

				binding = tcpBinding;
			} else {
				throw new ArgumentException("Invalid URI scheme");
			}
			
			var endPointAddress = new EndpointAddress(host);

			var context = new InstanceContext(this.ClientCallback);

			//*
			var endpoint = new ServiceEndpoint(ContractDescription.GetContract(typeof(ILobbyService)), binding, endPointAddress);			
			{
			    //remark: disposing the factory will close all created channels!
			    var channelFactory = new DuplexChannelFactory<ILobbyService>(context, endpoint);
			    this.LobbyService = channelFactory.CreateChannel();
			}
			{
			    var lobbyServiceCommObj = (ICommunicationObject)LobbyService;
			    CommObjWatcher.Create(lobbyServiceCommObj).StateChanged += LobbyServiceChannel_StateChanged;
			    lobbyServiceCommObj.Open();
			}
			return this.LobbyService.Enter(desiredNickname);
			/*/
			var client = new MyClient(context, tcpBinding, endPointAddress);
			//client.Open(); //is implicit

			CommObjWatcher.Create(client).StateChanged += LobbyServiceChannel_StateChanged;

			LobbyService = client.Service;


			return this.LobbyService.Enter(desiredNickname);
			//*/
		}

		public void Dispose()
		{
			this.Disconnect();
		}

		public void Disconnect()
		{
			var service = this.LobbyService;
			var channel = this.LobbyServiceChannel;

			if (channel != null) {
				//var debug = channel.State;
				//var debug = ((IDuplexContextChannel)service).AutomaticInputSessionShutdown;
				
				if (channel.State == CommunicationState.Opened) {
					try
					{
						if (service != null) service.Disconnect();
						channel.Close();
					} catch (CommunicationException ex) {
						OnCommunicationExceptionHappened(ex);
					}
				}

				if (channel.State != CommunicationState.Faulted) channel.Dispose();
			}

			this.LobbyService = DisconnectedLobbyService.Instance;
		}

		private void DisconnectChannel()
		{
		    var channel = this.LobbyServiceChannel;
		    if (channel != null) channel.Close();
		}

		private void OnClientEntered(ILobbyClientView client)
		{
			//TODO: Create a new type of Custom Event: OperationContextContinuationEvent

			OperationContext.Current.InvokeOnCompleted(
				() => this.ClientEntered.RaiseEvent(this, new EvArg<ILobbyClientView>(client))
			);
		}

		private void OnClientLeft(ILobbyClientView client)
		{
			OperationContext.Current.InvokeOnCompleted(
				() => this.ClientLeft.RaiseEvent(this, new EvArg<ILobbyClientView>(client))
			);
		}

		private void OnMessageReceive(IChatMessage message)
		{
			OperationContext.Current.InvokeOnCompleted(
				() => this.MessageReceived.RaiseEvent(this, new EvArg<IChatMessage>(message))
			);
		}

		private void OnDropped(DropReason reason)
		{
			OperationContext.Current.InvokeOnCompleted(
			    () => this.Dropped.RaiseEvent(this, new EvArg<DropReason>(reason))
			);
		}

		private void OnCommunicationExceptionHappened(CommunicationException exception)
		{
			System.Diagnostics.Debug.Write(exception, "CommunicationException");

			OperationContext.Current.InvokeOnCompleted(
				() => this.CommunicationExceptionHappened.RaiseEvent(this, new ThreadExceptionEventArgs(exception))
			);
		}

		private void LobbyServiceChannel_StateChanged(object sender, ValueChangedEventArgs<CommunicationState> e)
		{
			OperationContext.Current.InvokeOnCompleted(
				() => this.StateChanged.RaiseEvent(sender, e)
			);
		}
	}
}
