﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Data;
using JpLabs.Extensions;
using System.Windows.Threading;
using DistributedGame.Common;

namespace DistributedGame
{
	public class ValueChangedEventArgs<T> : EventArgs
	{
		public ValueChangedEventArgs(T oldValue, T newValue)
		{
			OldValue = oldValue;
			NewValue = newValue;
		}
		
		public T NewValue { get; private set; }
		public T OldValue { get; private set; }
		
		private bool? didValueChanged;
		public bool DidValueChanged
		{
			get {
				if (didValueChanged == null) didValueChanged = object.Equals(OldValue, NewValue);
				return didValueChanged.Value;
			}
		}
	}

	internal class CommObjWatcher
	{
		private readonly ICommunicationObject commObj;
		private CommunicationState lastState;
		
		private CommObjWatcher(ICommunicationObject commObj)
		{
			this.commObj = commObj;
			this.lastState = commObj.State;

			commObj.Closed	+= delegate { this.OnStateChanged(CommunicationState.Closed); };
			commObj.Closing	+= delegate { this.OnStateChanged(CommunicationState.Closing); };
			commObj.Faulted	+= delegate { this.OnStateChanged(CommunicationState.Faulted); };
			commObj.Opened	+= delegate { this.OnStateChanged(CommunicationState.Opened); };
			commObj.Opening	+= delegate { this.OnStateChanged(CommunicationState.Opening); };
		}

		public static CommObjWatcher Create(ICommunicationObject commObj)
		{
			return new CommObjWatcher(commObj);
		}

		private void OnStateChanged(CommunicationState newState)
		{
			var evArgs = new ValueChangedEventArgs<CommunicationState>(lastState, newState);
			this.lastState = newState;
			StateChanged.RaiseEvent(commObj, evArgs);
		}

		public event EventHandler<ValueChangedEventArgs<CommunicationState>> StateChanged;
	}

	internal static class MessageFormatter
	{
		public static string WelcomeMessage(string nickname)
		{
			return string.Format("Welcome, {0}!", nickname);
		}

		public static string ClientJoinedMessage(string nickname)
		{
			return string.Format("{0} has joined", nickname);
		}

		//public static string HostMessage(string text, string hostName)
		//{
		//    return string.Format("{{{0}}} {1}", hostName, text);
		//}

		//public static string ClientMessage(string text, string clientNickname)
		//{
		//    return string.Format("<{0}> {1}", clientNickname, text);
		//}

		public static string ClientIsDisconnectingMessage(string nickname)
		{
			return string.Format("* {0} is disconnecting", nickname);
		}

		public static string Message(IChatMessage message)
		{
			string format = "<{0}> {1}"; //(message.Sender.IsHost) ? "{{{0}}} {1}" : "<{0}> {1}";
			return string.Format(format, message.Sender.Nickname, message.Content);

			//var clientMessage = message as ClientMessage;
			//if (clientMessage != null) {
			//    string format = (clientMessage.Sender.IsHost) ? "{{{0}}} {1}" : "<{0}> {1}";
			//    return string.Format(format, clientMessage.Sender.Nickname, clientMessage.Content);
			//} else {
			//    return string.Concat("* ", message.Content);
			//}
		}
	}

	//[ServiceBehavior()]
	//public class RemoteCollection<T> : IRemoteCollection //MarshalByRefObject, ICollection<T>
	//{
	//    private readonly ICollection<T> innerCollection;

	//    public RemoteCollection(ICollection<T> source)
	//    {
	//        innerCollection = source;
	//    }

	//    int IRemoteCollection.GetCount()
	//    {
	//        return innerCollection.Count;
	//    }

	//    //public void Add(T item)
	//    //{
	//    //    innerCollection.Add(item);
	//    //}

	//    //public void Clear()
	//    //{
	//    //    innerCollection.Clear();
	//    //}

	//    //public bool Contains(T item)
	//    //{
	//    //    return innerCollection.Contains(item);
	//    //}

	//    //public void CopyTo(T[] array, int arrayIndex)
	//    //{
	//    //    innerCollection.CopyTo(array, arrayIndex);
	//    //}

	//    //public int Count
	//    //{
	//    //    get { return innerCollection.Count; }
	//    //}

	//    //public bool IsReadOnly
	//    //{
	//    //    get { return innerCollection.IsReadOnly; }
	//    //}

	//    //public bool Remove(T item)
	//    //{
	//    //    return innerCollection.Remove(item);
	//    //}

	//    //public IEnumerator<T> GetEnumerator()
	//    //{
	//    //    return innerCollection.GetEnumerator();
	//    //}

	//    //System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
	//    //{
	//    //    return innerCollection.GetEnumerator();
	//    //}
	//}


    /// <summary> 
    /// Represents a dynamic data collection that provides notifications when items get added, removed, or when the whole list is refreshed. 
    /// </summary> 
    /// <typeparam name="T"></typeparam>
    internal class ObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T>
    {
        /// <summary> 
        /// Adds the elements of the specified collection to the end of the ObservableCollection(Of T). 
        /// </summary> 
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var i in collection) Items.Add(i);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary> 
        /// Removes the first occurence of each item in the specified collection from ObservableCollection(Of T). 
        /// </summary> 
        public void RemoveRange(IEnumerable<T> collection)
        {
            foreach (var i in collection) Items.Remove(i);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary> 
        /// Clears the current collection and replaces it with the specified item. 
        /// </summary> 
        public void Replace(T item)
        {
            ReplaceRange(new T[] { item });
        }
        /// <summary> 
        /// Clears the current collection and replaces it with the specified collection. 
        /// </summary> 
        public void ReplaceRange(IEnumerable<T> collection)
        {
            //List<T> old = new List<T>(this);
            Items.Clear();
            foreach (var i in collection) Items.Add(i);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));//, null, old));
        }

        /// <summary> 
        /// Initializes a new instance of the System.Collections.ObjectModel.ObservableCollection(Of T) class. 
        /// </summary> 
        public ObservableCollection()
            : base() { }

		/// <summary> 
		/// Initializes a new instance of the System.Collections.ObjectModel.ObservableCollection(Of T) class that contains elements copied from the specified collection. 
		/// </summary> 
		/// <param name="collection">collection: The collection from which the elements are copied.</param> 
		/// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception> 
		public ObservableCollection(IEnumerable<T> collection)
		    : base(collection) { }

		//public event NotifyCollectionChangedEventHandler CollectionChanged;

		//private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		//{
		//    var handler = this.CollectionChanged;
		//    if (handler != null) handler(this, e);
		//}
	}
}
