﻿using System;
using System.Collections;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using DistributedGame.Client;
using DistributedGame.Common;
using JpLabs.Extensions;
using JpLabs.TweakedEvents;
using System.Collections.Generic;

namespace DistributedGame
{
	/// <summary>
	/// Interaction logic for ClientPage.xaml
	/// </summary>
	public partial class ClientPage : Page, IDisposable
	{
		public Uri RemoteHostAddress { get; set; }
		
		private LobbyClient LobbyClient;

		public CommunicationState ClientState
		{
			get {
				var client = this.LobbyClient;
				if (client == null) return CommunicationState.Closed;
				return client.State;
			}
		}
		
		public ClientPage()
		{
			InitializeComponent();

			txtChatArea.Document.LineHeight = 1;

			//this.Unloaded += new RoutedEventHandler(ClientPage_Unloaded);
			Application.Current.Exit += TweakedEvent.ToWeak<ExitEventHandler>(CurrentApplication_Exit);

			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
		}

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("Unhadled Exception: {0}", e.ExceptionObject.ToString());
		}

		private void CurrentApplication_Exit(object sender, ExitEventArgs e)
		{
			Dispose();
		}

		//private void ClientPage_Unloaded(object sender, RoutedEventArgs e)
		//{
		//    Dispose();
		//}

		public void Dispose()
		{
			var client = this.LobbyClient;
			if (client != null) client.Dispose();

			this.LobbyClient = null;
		}
		
		private void txtChat_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter) {
				string text = txtChat.Text;
				txtChat.Text = null;
				
				TextEntered(text);
			}
		}
		
		private void btnConnect_Click(object sender, RoutedEventArgs e)
		{
			var state = this.ClientState;
			switch (state)
			{
				case CommunicationState.Closed: ConnectTo(this.RemoteHostAddress); break;
				//case CommunicationState.Created: break;
				//case CommunicationState.Opened:
				default: Disconnect(); break;
			}
			
			UpdateUI();
		}

		private void ConnectTo(Uri remoteHostAddress)
		{
			LobbyClient = new LobbyClient();

			AttachEvents(LobbyClient);
			
			//var clientInfo = GameClient.Connect(remoteHostAddress, txtName.Text, this);
			//AddChatText("Connected to " + this.RemoteHostAddress);
			//AddChatText("as '" + clientInfo.Nickname + "'");

			AddChatText("Conecting...");

			string desiredNickname = txtName.Text;
			Task.Factory.StartNew(
				() => {
					ILobbyClientView clientView;
					try 
					{
						clientView = LobbyClient.Enter(remoteHostAddress, desiredNickname);

						this.AddChatText(string.Format("Connected to {0} as '{1}'", remoteHostAddress, clientView.Nickname));

						Dispatcher.CheckAndInvoke( () => { txtName.Text = clientView.Nickname; } );

						UpdatePlayerList();
					} catch (Exception ex) {
						this.AddChatText("Error connecting: " + ex.Message);
					}

					UpdateUI();
				}
			)
			.ContinueWith(LogTaskException, TaskContinuationOptions.OnlyOnFaulted);			
		}

		private void AttachEvents(LobbyClient lobbyClient)
		{
			lobbyClient.StateChanged += TweakedEvent.ToWeak<ValueChangedEventArgs<CommunicationState>>(LobbyClient_StateChanged);
			lobbyClient.CommunicationExceptionHappened += TweakedEvent.ToWeak<ThreadExceptionEventArgs>(LobbyClient_CommunicationExceptionHappened);

			lobbyClient.ClientEntered += TweakedEvent.ToWeak<EvArg<ILobbyClientView>>(Lobby_ClientEntered);
			lobbyClient.ClientLeft	+= TweakedEvent.ToWeak<EvArg<ILobbyClientView>>(Lobby_ClientLeft);
			lobbyClient.MessageReceived += TweakedEvent.ToWeak<EvArg<IChatMessage>>(LobbyClient_MessageReceived);
			lobbyClient.Dropped += TweakedEvent.ToWeak<EvArg<DropReason>>(LobbyClient_Dropped);
		}

		private void Disconnect()
		{
			this.Dispose();
			
			this.AddChatText("* Disconnected");

			UpdateUI();
		}

		void LobbyClient_StateChanged(object sender, ValueChangedEventArgs<CommunicationState> e)
		{
			Dispatcher.BeginInvoke(
				DispatcherPriority.Normal,
				delegate {
					AddChatText("* Channel " + e.NewValue);
					UpdateUI();
				}
			);
		}

		void LobbyClient_CommunicationExceptionHappened(object sender, ThreadExceptionEventArgs e)
		{
			BeginAddChatText(string.Format("* Exception: {0}", e.Exception.Message));
		}

		void Lobby_ClientEntered(object sender, EvArg<ILobbyClientView> e)
		{
			BeginAddChatText(string.Format("* {0} entered", e.Value.Nickname));

			BeginUpdatePlayerList();
		}

		void Lobby_ClientLeft(object sender, EvArg<ILobbyClientView> e)
		{
			BeginAddChatText(string.Format("* {0} left", e.Value.Nickname));

			BeginUpdatePlayerList();
		}

		void LobbyClient_MessageReceived(object sender, EvArg<IChatMessage> e)
		{
			BeginAddChatText(MessageFormatter.Message(e.Value));
		}

		void LobbyClient_Dropped(object sender, EvArg<DropReason> e)
		{
			BeginAddChatText("* Connection dropped: " + e.Value.Description);

			BeginUpdatePlayerList();

			this.Disconnect();
		}

		private DispatcherOperation BeginUpdatePlayerList()
		{
			return Dispatcher.BeginInvoke(DispatcherPriority.DataBind, UpdatePlayerList);
		}

		private void UpdatePlayerList()
		{
			//TODO: Avoid concurrent calls to ILobby.get_Clients
			// - Make it run asynchronously;
			// - Follow this pattern: InvalidatePlayerList()
			// - Try this: http://bindablelinq.codeplex.com/

			var currentClient = LobbyClient;
			if (currentClient == null) {
				Dispatcher.CheckAndInvoke( () =>
					lstPlayerList.ItemsSource = EmptyReadOnlyColl<string>.Value
				);
			} else {
				var futureClientListTask = Task.Factory.StartNew( () => currentClient.LobbyService.GetClientList().Select( c => c.Nickname ) );

				lstPlayerList.SetFutureProperty(ItemsControl.ItemsSourceProperty, futureClientListTask);
			}
		}

		private static void LogTaskException(Task task)
		{
		    System.Diagnostics.Debug.WriteLine(task.Exception);
		    if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
		}
		
		private void UpdateUI()
		{
			if (Dispatcher.TryInvoke(UpdateUI)) return;

			var state = this.ClientState;

			txtName.IsReadOnly = true;
			btnConnectDisconnect.IsEnabled = false;

			switch (state)
			{
				case CommunicationState.Closed: {
					btnConnectDisconnect.Content = "Connect";
					btnConnectDisconnect.IsEnabled = true;
					txtName.IsReadOnly = false;
				} break;

				case CommunicationState.Created:
				case CommunicationState.Opening: {
					btnConnectDisconnect.Content = "Connecting...";
				} break;

				case CommunicationState.Opened: {
					btnConnectDisconnect.Content = "Disconnect";
					btnConnectDisconnect.IsEnabled = true;

					//lstPlayerList.ItemsSource = this.GameClient.ClientCallback.Clients; //new System.Collections.ObjectModel.ObservableCollection<string>();

					//CollectionView

					//Binding

					//ItemsControl.ItemsSourceProperty.s
					//this.GameClient.GameHost.Subscribe( (sender, e) => { MessageBox.Show("received"); } );


				} break;

				case CommunicationState.Faulted: {
					btnConnectDisconnect.Content = "Faulted - Dispose";
					btnConnectDisconnect.IsEnabled = true;
				} break;

				default: {
					btnConnectDisconnect.Content = state.ToString();
				} break;
			}

		}

		private void TextEntered(string text)
		{
			if (this.ClientState == CommunicationState.Opened) {
				LobbyClient.LobbyService.Say(text);
			} else {
				AddChatText("[offline] " + text);
					
				//var cancelSource = new CancellationTokenSource();
				//var task = Task.Factory.StartNew(
				//    () => {
				//        System.Threading.Thread.Sleep(2000);
				//        //throw new InvalidOperationException();
				//        cancelSource.Token.ThrowIfCancellationRequested();

				//        return new [] { text };
				//    }
				//    ,cancelSource.Token
				//);

				//var binding = TaskBinding.FromTask(
				//    task,
				//    t => t.Status.ToEnumerable()
				//);

				//cancelSource.Cancel();

				//lstPlayerList.SetBinding(ItemsControl.ItemsSourceProperty, binding);
			}
		}
		
		private DispatcherOperation BeginAddChatText(string text)
		{
			return Dispatcher.BeginInvoke(DispatcherPriority.Normal, AddChatText, text);
		}

		private void AddChatText(string text)
		{
			if (Dispatcher.TryInvoke(AddChatText, text)) return;

			txtChatArea.AppendText(text + Environment.NewLine);
			txtChatArea.ScrollToEnd();
		}

		//void IGameClientCallback.Receive(RemoteMessage message)
		//{
		//    var text = MessageFormatter.Message(message);
		//    this.Dispatcher.InvokeIfNeeded( (Action<string>)AddChatText, text );
		//}

		////void IGameClientCallback.RefreshClients(IList<ClientConnectionInfo> clients)
		////{
		////    lstPlayerList.ItemsSource = clients.Select( c => c.Nickname ).ToArray();
		////}

		//void IGameClientCallback.ServerClosing()
		//{
		//    this.Dispatcher.InvokeIfNeeded( (Action<string>)AddChatText, "* Host is closing" );

		//    //WCF doesn't closes the channel on the client side for some reason (reconnect?)
		//    this.GameClient.GameChannel.Close();
		//}
	}
}
