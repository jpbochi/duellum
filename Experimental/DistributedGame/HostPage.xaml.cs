﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;
using DistributedGame.Common;
using DistributedGame.Server;
using JpLabs.Extensions;
using JpLabs.TweakedEvents;

namespace DistributedGame
{
	/// <summary>
	/// Interaction logic for HostPage.xaml
	/// </summary>
	public partial class HostPage : Page, IDisposable
	{
		public Uri HostAddress { get; set; }
		
		private ILobby Lobby;
		private ILobbyClientTicket LobbyTicket;
		private LobbyServer LobbyServer;
		
		public HostPage()
		{
			InitializeComponent();

			txtChatArea.Document.LineHeight = 1;

			Application.Current.Exit += TweakedEvent.ToWeak<ExitEventHandler>(CurrentApplication_Exit);

			//this.NavigationService.Navigating += TweakedEvent.ToWeak<NavigatingCancelEventHandler>(NavigationService_Navigating);
			//NavigationService.GetNavigationService(this).Navigating += TweakedEvent.ToWeak<NavigatingCancelEventHandler>(NavigationService_Navigating);

			//this.Dispatcher.UnhandledException += TweakedEvent.ToWeak<DispatcherUnhandledExceptionEventHandler>(Dispatcher_UnhandledException);
		}

		private void CurrentApplication_Exit(object sender, ExitEventArgs e)
		{
			Dispose();
		}

		private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
		{
		    if (this.ServerState == CommunicationState.Opened) {
		        var response = MessageBox.Show(
		            Window.GetWindow(this),
		            "This action will close the server. Are you sure you want that?",
		            "Close Server",
		            MessageBoxButton.YesNo
		        );

		        if (response == MessageBoxResult.No) {
		            e.Cancel = true;
		            return;
		        }
		    }

		    this.Dispose();
		}

		public CommunicationState ServerState
		{
			get {
				var server = this.LobbyServer;
				if (server == null) return CommunicationState.Closed;
				return server.State;
			}
		}

		private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			try
			{
				AddChatText(e.Exception.ToString());
				e.Handled = true;
			}
			catch {}
		}

		private void txtChat_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter) {
				string text = txtChat.Text;
				txtChat.Text = null;
				
				TextEntered(text);
			}
		}

		private void TextEntered(string text)
		{
			var ticket = this.LobbyTicket;
			if (ticket != null) {
				ticket.Say(text);
			} else {
				AddChatText("[offline]: " + text);
			}
		}
		
		private void AddChatText(string text)
		{
			if (Dispatcher.TryInvoke(AddChatText, text)) return;

			txtChatArea.AppendText(text + Environment.NewLine);
			txtChatArea.ScrollToEnd();
		}

		private void btnStartStop_Click(object sender, RoutedEventArgs e)
		{
			var state = this.ServerState;
			switch (state)
			{
				case CommunicationState.Closed: StartServer(this.HostAddress); break;
				case CommunicationState.Created: break;
				default: StopServer(); break;
			}

			UpdateUI();
		}

		private void StartServer(Uri address)
		{
			this.Lobby = new Lobby();
			this.LobbyTicket =  this.Lobby.Enroll(string.Format("{{{0}}}", this.txtName.Text));
			this.LobbyServer = new LobbyServer(this.Lobby);

			AttachEvents(this.Lobby);
			AttachEvents(this.LobbyServer);

			AddChatText("Starting server...");

			UpdateUI();

			Task.Factory.StartNew(
			    () => this.LobbyServer.StartServer(address.Port)//address)
			)
			.ContinueWith(
			    t => {
			        if (t.IsFaulted) {
						//System.Diagnostics.Debugger.Break();

			            AddChatText("Error starting server: " + t.Exception.InnerException.Message);
			        } else {
			            AttachEvents(this.LobbyTicket);

			            AddChatText("Server open at " + address.ToString());
			        }
			        UpdateUI();
			    }
			);
		}

		private void StopServer()
		{
			AddChatText("Stopping server...");

			Dispose();

			AddChatText("Server stopped");
		}

		public void Dispose()
		{
			IDisposable disposable;

			disposable = this.LobbyTicket as IDisposable;
			if (disposable != null) disposable.Dispose();

			disposable = this.Lobby as IDisposable;
			if (disposable != null) disposable.Dispose();
			
			disposable = this.LobbyServer as IDisposable;
			if (disposable != null) disposable.Dispose();

			this.Lobby = null;
			this.LobbyTicket = null;
			this.LobbyServer = null;
		}

		private void UpdateUI()
		{
			if (Dispatcher.TryInvoke(UpdateUI)) return;

			var state = this.ServerState;

			txtName.IsReadOnly = true;
			btnStartStop.IsEnabled = false;

			//ICommand

			switch (state)
			{
				case CommunicationState.Closed: {
					btnStartStop.Content = "Start Server";
					txtName.IsReadOnly = false;
					btnStartStop.IsEnabled = true;
				} break;

				case CommunicationState.Created:
				case CommunicationState.Opening: {
					btnStartStop.Content = "Starting...";
				} break;

				case CommunicationState.Opened: {
					btnStartStop.Content = "Stop Server";
					btnStartStop.IsEnabled = true;
				} break;

				case CommunicationState.Faulted: {
					btnStartStop.Content = "Faulted - Dispose";
					btnStartStop.IsEnabled = true;
				} break;

				default: {
					btnStartStop.Content = state.ToString();
				} break;
			}

			UpdatePlayerListAsync();
		}

		private void UpdatePlayerListAsync()
		{
			Dispatcher.BeginInvoke(DispatcherPriority.DataBind, UpdatePlayerList);
		}

		private void UpdatePlayerList()
		{
			if (Dispatcher.TryInvoke(UpdatePlayerList)) return;

			var currentLobby = this.Lobby;

			if (currentLobby == null)
				UpdatePlayerList(EmptyReadOnlyColl<ILobbyClientView>.Value);
			else
				UpdatePlayerList(currentLobby.Clients);
		}

		private void UpdatePlayerList(IEnumerable<ILobbyClientView> clients)
		{
			lstClientList.ItemsSource = clients.Select( p => p.Nickname ).ToArray();
		}

		private void AttachEvents(LobbyServer lobbyServer)
		{
			lobbyServer.CommunicationExceptionHappened += TweakedEvent.ToWeak<ThreadExceptionEventArgs>(LobbyServer_CommunicationExceptionHappened);
		}

		private void AttachEvents(ILobby lobby)
		{
			lobby.ClientEntered	+= TweakedEvent.ToWeak<EvArg<ILobbyClientView>>(Lobby_ClientEntered);
			lobby.ClientLeft	+= TweakedEvent.ToWeak<EvArg<ILobbyClientView>>(Lobby_ClientLeft);
		}

		private void AttachEvents(ILobbyClientTicket ticket)
		{
			ticket.MessageReceived += TweakedEvent.ToWeak<EvArg<IChatMessage>>(Ticket_MessageReceived);

			//host.ClientConnected		+= TweakedEvent.ToWeakSynced<ClientSessionEventArgs>(this, Lobby_ClientEntered);
			//host.ClientDisconnected		+= TweakedEvent.ToWeakSynced<ClientSessionEventArgs>(this, GameHost_ClientDisconnected);
			//host.ClientChannelClosed	+= TweakedEvent.ToWeakSynced<ClientSessionEventArgs>(this, GameHost_ClientChannelClosed);
			//host.ClientChannelFaulted	+= TweakedEvent.ToWeakSynced<ClientSessionEventArgs>(this, GameHost_ClientChannelFaulted);
			//host.MessageReceived		+= TweakedEvent.ToWeakSynced<RemoteMessageEventArgs>(this, GameHost_MessageReceived);
		}

		void LobbyServer_CommunicationExceptionHappened(object sender, ThreadExceptionEventArgs e)
		{
			BeginAddChatText(string.Format("* Exception: {0}", e.Exception.Message));
		}

		void Lobby_ClientEntered(object sender, EvArg<ILobbyClientView> e)
		{
			BeginAddChatText(string.Format("* {0} entered", e.Value.Nickname));

			UpdatePlayerListAsync();
		}

		void Lobby_ClientLeft(object sender, EvArg<ILobbyClientView> e)
		{
			BeginAddChatText(string.Format("* {0} left", e.Value.Nickname));

			UpdatePlayerListAsync();
		}

		void Ticket_MessageReceived(object sender, EvArg<IChatMessage> e)
		{
			BeginAddChatText(MessageFormatter.Message(e.Value));
		}

		private DispatcherOperation BeginAddChatText(string text)
		{
			return Dispatcher.BeginInvoke(DispatcherPriority.Normal, AddChatText, text);
		}
	}
}
