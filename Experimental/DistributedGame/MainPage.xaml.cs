﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DistributedGame
{
	/// <summary>
	/// Interaction logic for MainPage.xaml
	/// </summary>
	public partial class MainPage : Page
	{
		public MainPage()
		{
			InitializeComponent();
		}

		//*

		private void StartHost_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(
				new HostPage() { HostAddress = new Uri(txtHostAddress.Text) }
			);
		}

		private void ConnectToHost_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(
				new ClientPage() { RemoteHostAddress = new Uri(txtHostAddress.Text) }
			);
		}

		/*/
		private void Go_Click(object sender, RoutedEventArgs e)
		{
			if (radStartHost.IsChecked == true) {
				this.NavigationService.Navigate(
					new HostPage() { HostAddress = new Uri(txtHostAddress.Text) }
				);
			} else {
				this.NavigationService.Navigate(
					new ClientPage() { RemoteHostAddress = new Uri(txtHostAddress.Text) }
				);
			}
		}
		//*/
	}
}
