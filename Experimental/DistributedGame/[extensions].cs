﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using JpLabs.Extensions;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Collections;

namespace DistributedGame
{
	public class TaskBinding
	{
		private const string ValuePropertyName = "Value";

		public static Binding FromTask<T>(Task<T> task, Func<Task<T>,object> temporaryValueFunc)
		{
			return new Binding() {
				Source = TaskBindingSource.FromTask(task, temporaryValueFunc),
				Path = new PropertyPath(ValuePropertyName),
				Mode = BindingMode.OneWay
			};
		}

		private class TaskBindingSource : INotifyPropertyChanged
		{
			public event PropertyChangedEventHandler PropertyChanged;

			public object Value { get; private set; }

			private TaskBindingSource()
			{}

			public static INotifyPropertyChanged FromTask<T>(Task<T> task, Func<Task<T>,object> temporaryValueFunc)
			{
				var source = new TaskBindingSource();

				source.Value = GetValue(task, temporaryValueFunc);

				task.ContinueWith(
					t => {
						//REMARK: task`s exceptions must always be observed or this exception will be thrown:
						//"A Task's exception(s) were not observed either by Waiting on the Task or accessing its Exception property. As a result, the unobserved exception was rethrown by the finalizer thread."
						System.Diagnostics.Debug.WriteLine(task.Exception);

						source.Value = GetValue(task, temporaryValueFunc);
						source.OnPropertyChanged(new PropertyChangedEventArgs(ValuePropertyName));
					}
				);

				return source;
			}

			private static object GetValue<T>(Task<T> task, Func<Task<T>,object> temporaryValueFunc)
			{
				if (!task.IsCompleted || task.IsFaulted || task.IsCanceled) return temporaryValueFunc(task);
				return task.Result;
			}

			private void OnPropertyChanged(PropertyChangedEventArgs e)
			{
				var handler = this.PropertyChanged;
				if (handler != null) handler(this, e);
			}
		}
	}

	public static class WpfControlExt
	{
		public static void SetFutureProperty<T>(this Control control, DependencyProperty property, Task<T> future)
		{
			Func<Task<T>,object> getTempValueFunc;

			if (typeof(IEnumerable).IsLittlerOrEqual(typeof(T))) {
				Func<IEnumerable> getItemsSource = () => control.GetValue(property) as IEnumerable;
				getTempValueFunc = t => (t.IsFaulted || t.IsCanceled) ? t.Status.ToEnumerable() : control.Dispatcher.CheckAndInvoke(getItemsSource);
			} else {
				Func<object> getItemsSource = () => control.GetValue(property);
				getTempValueFunc = t => (t.IsFaulted || t.IsCanceled) ? t.Status : control.Dispatcher.CheckAndInvoke(getItemsSource);
			}

			var futureBinding = TaskBinding.FromTask(future, getTempValueFunc);

			control.Dispatcher.CheckAndInvoke( () => 
				control.SetBinding(property, futureBinding)
			);
		}
	}

	public static class DispatcherExt
	{
		public static bool TryInvoke(this Dispatcher dispatcher, Action dlg)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg);
			return true;
		}

		public static bool TryInvoke<T>(this Dispatcher dispatcher, Action<T> dlg, T arg)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg, arg);
			return true;
		}

		public static bool TryInvoke<T1,T2>(this Dispatcher dispatcher, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			if (dispatcher.CheckAccess()) return false;
			InternalInvoke(dispatcher, dlg, arg1, arg2);
			return true;
		}

		public static DispatcherOperation BeginInvoke(this Dispatcher dispatcher, DispatcherPriority priority, Action dlg)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg);
		}

		public static DispatcherOperation BeginInvoke<T>(this Dispatcher dispatcher, DispatcherPriority priority, Action<T> dlg, T arg)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg, arg);
		}

		public static DispatcherOperation BeginInvoke<T1,T2>(this Dispatcher dispatcher, DispatcherPriority priority, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			return InternalBeginInvoke(dispatcher, priority, dlg, arg1, arg2);
		}

		public static void CheckAndInvoke(this Dispatcher dispatcher, Action dlg)
		{
			InternalCheckAndInvoke(dispatcher, dlg);
		}

		public static void CheckAndInvoke<T>(this Dispatcher dispatcher, Action<T> dlg, T arg)
		{
			InternalInvoke(dispatcher, dlg, arg);
		}

		public static void CheckAndInvoke<T1,T2>(this Dispatcher dispatcher, Action<T1,T2> dlg, T1 arg1, T2 arg2)
		{
			InternalCheckAndInvoke(dispatcher, dlg, arg1, arg2);
		}

		public static TOut CheckAndInvoke<TOut>(this Dispatcher dispatcher, Func<TOut> dlg)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg);
		}

		public static TOut CheckAndInvoke<T,TOut>(this Dispatcher dispatcher, Func<T,TOut> dlg, T arg)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg, arg);
		}

		public static TOut CheckAndInvoke<T1,T2,TOut>(this Dispatcher dispatcher, Func<T1,T2,TOut> dlg, T1 arg1, T2 arg2)
		{
			return (TOut)InternalCheckAndInvoke(dispatcher, dlg, arg1, arg2);
		}

		private static object InternalInvoke(Dispatcher dispatcher, Delegate dlg, params object[] args)
		{
				return dispatcher.Invoke(dlg, DispatcherPriority.Normal, args);
		}

		private static DispatcherOperation InternalBeginInvoke(Dispatcher dispatcher, DispatcherPriority priority, Delegate dlg, params object[] args)
		{
			return dispatcher.BeginInvoke(dlg, priority, args);
		}

		private static object InternalCheckAndInvoke(Dispatcher dispatcher, Delegate dlg, params object[] args)
		{
			if (!dispatcher.CheckAccess()) return dispatcher.Invoke(dlg, DispatcherPriority.Normal, args);
			
			return dlg.DynamicInvoke(args); //TODO:[optimize] DynamicInvoke is slow
		}
	}
}
