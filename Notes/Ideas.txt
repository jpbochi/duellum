- Basic Changes
	- Ghost: Bullets should pass through him, and he might suffer less damage
- Arcane Spells
	- Time Stop (Extra time to do anything with no record of actions)
	- Cast Hologram
	- Open Doors
	- Arcane Shield
	- Permute positions with another subject
	- Undetection (blocks light/sound/smell...)
	- Magic Darts (dardos m�sticos) (shouldn't it be a weapon?)
	- Steal Fog of War
- Divine Spells
	- Pray
	- Heal
	- Thunder
	- Invoke Creature/Objects
	- Vision of Truth (Greatly enhances all senses for a short time)
	- Inspect Subject (see real name, health, etc.)
- Powers
	- Dementor (from Harry Potter)
		- Death Kiss: kill anyone for good (Jason wouldn't be able to ressurect)
		- Fear Aura
	- Vampire
		- Option to change between two forms (Bat/Normal)
		- Blood sucking attack (?)
	- Werewolf
		- Alternates between two forms (Wolf/Normal) without player's control
		- How to make player unaware of his werewolf condition?
	- Medusa
		- Petrification
	- Verme Maldito (could be a bot)
	- Unique Powers
		- The Jackal (Sniper)
		- Sir Ernest Shackleton (Casca Grossa)
		- Rambo (Boina Verde)
		- Elminster or Gandalf (Mage)
		- Tiger I (Tank)
		- Agent Smith (Matrix Agent)
		- Luke Skywalker (Jedi)
		- Smaug (Dragon)
		- Max Payne (PM)
		- Irm�os Metralha (Capangas)
		- T-X (T-1000)
		- Ethereal Commander (Ethereal)
		- Fzoul Chembryl (Evil Priest)
		- Hattori Hanzo (Ninja)
		- Ironman (Blindado)
		- Usain Bolt (Runner)
		- Marshall James Anderson (?)
- Unique Powers
- Remote Sensors
	- It's an entity with senses of its own
	- Remote mode:
		- According to specific rules (eg: found a remote control artifact), players will be able to switch on/off remote mode
		- While in remote mode, players will have it's own senses reduced (or completely turned off) and use remote senses
	- Remote Controlled Entities
		- Player can give commands to controlled entity
		- Entity can have weapons/equipment/powers as well as senses
		- Does not necessarily have an owner, i.e., two players can fight for control
			- Entity must not be able to spend more than its allowed time units per turn (how? )
	- Vehicles
		- Similar to RCE, except that they are not necessarily remote
		- Entities can get on board of a vehicle
- Automata
	- Players can deploy/create/summon them and set simple tasks like: go to a given position and explode
	- Each automaton will have a turn of its own
	- Examples: Golems, Zombis, Search and Destroy Robots