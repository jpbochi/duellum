﻿using System;
using System.IO;

namespace Duellum.PackagerCmd
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length != 2) {
				Console.WriteLine("Two arguments are expected");
				Console.WriteLine(" 1. package source root path");
				Console.WriteLine(" 2. package destination file name");
				PressAnyKeyDebug();
				return;
			}
			
			//string rootPath = @"C:\JP\Dev\Duelo\Duellum\Wpf2dImgs\";
			//string packagePath = @"C:\JP\Dev\Duelo\Duellum\Wpf2dImgs.dlpx";
			string rootPath = MakeAbsolutePath(args[0]);
			string packagePath = MakeAbsolutePath(args[1]);

			Console.WriteLine("Package Source Root Path is '{0}'", rootPath);
			Console.WriteLine("Package Destination file is '{0}'", packagePath);
			Console.WriteLine("Creating package . . .");
			
			Packager.CreatePackage(packagePath, rootPath);
			
			Console.WriteLine("Package created !");
			PressAnyKey();
		}

		static public string MakeAbsolutePath(string relativePath)
		{
			if (Path.IsPathRooted(relativePath)) return relativePath;
			//return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
			return Path.Combine(Environment.CurrentDirectory, relativePath);
		}
		
		[System.Diagnostics.Conditional("DEBUG")]
		static void PressAnyKeyDebug()
		{
			PressAnyKey();
		}
		
		static void PressAnyKey()
		{
			Console.Write("Press any key to continue . . .");
			Console.ReadKey(true);
		}
	}
}
