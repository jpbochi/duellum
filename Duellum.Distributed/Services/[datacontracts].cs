﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Duellum.Distributed.Services
{
	/// TODO:
	/// - Authentication / Authorization
	/// - Connection keep-alive [DONE - http://www.smartasses.be/2009/01/26/wcf-reliable-session-and-keep-alives/ ]
	/// - Client reconnect after fault (i.e., keeps game state and player slot until Server decides to close it)
	/// - Separate concepts of GameClient and GamePlayer (e.g., two players can play from the same remote client)
	/// - Handle exceptions that occur on asynchronous events

    [Serializable,DataContract]
    public class ClientConnectRequest
    {
        [DataMember]
        public string DesiredNickname { get; internal set; }
    }

    [Serializable,DataContract]
    public class ClientConnectionInfo
    {
		//use [NonSerialized] to hide temp fields

		public static readonly string HOST_ID = "{host}";
		public bool IsHost { get { return SessionId == HOST_ID; } }

        [DataMember]
        public string SessionId { get; internal set; }
		
        [DataMember]
        public string Nickname { get; set; } //TODO: players should be able to rename themselves
        
        /// Possible other properties:
		/// - Network Address (URI)
		/// - Client Version
		/// - DateTime connected
    }

	[Serializable,DataContract]
	[ImmutableObject(true)]
	[KnownType(typeof(ClientMessage))] //http://blogs.msdn.com/youssefm/archive/2009/04/21/understanding-known-types.aspx
	public class RemoteMessage
	{
	    [DataMember]
	    public string Content { get; internal set; }
        
	    [DataMember]
	    public DateTime Time { get; internal set; }

		public RemoteMessage(string content)
		{
			this.Time = DateTime.Now;
			this.Content = content;
		}
	}

	[Serializable,DataContract]
	[ImmutableObject(true)]
	public class ClientMessage : RemoteMessage
	{
	    [DataMember]
	    public ClientConnectionInfo Sender { get; internal set; }

		public ClientMessage(ClientConnectionInfo sender, string content) : base(content)
		{
			this.Sender = sender;
		}
	}
}
