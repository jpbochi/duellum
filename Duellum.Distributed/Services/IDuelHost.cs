﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Duellum.Distributed.Services
{
    [ServiceContract(CallbackContract = typeof(IDuelClientCallback), SessionMode = SessionMode.Required)]
    public interface IDuelHost
    {
		[OperationContract(IsInitiating = true)]
		ClientConnectionInfo Connect(ClientConnectRequest request);

		[OperationContract(IsOneWay = true)]
		void Say(string message);

		//[OperationContract(IsOneWay = true)]
		//void Whisper(string msg, ClientInfo receiver);

		//[OperationContract(IsOneWay = true)]
		//void IsWriting(ClientInfo client);

		[OperationContract(IsOneWay = true, IsTerminating = true)]
		void Disconnect();
    }

    public interface IDuelClientCallback
    {
		[OperationContract(IsOneWay = true)]
		void Receive(RemoteMessage message);

		[OperationContract(IsOneWay = true)]
		void RefreshClients(IList<ClientConnectionInfo> clients);

		//[OperationContract(IsOneWay = true)]
		//void Receive(Message message);

		//[OperationContract(IsOneWay = true)]
		//void ClientIsTyping(ClientInfo client);

		//[OperationContract(IsOneWay = true)]
		//void ClientJoined(ClientInfo client);

		//[OperationContract(IsOneWay = true)]
		//void ClientLeft(ClientInfo client);

		[OperationContract(IsOneWay = true, IsTerminating = true)]
		//[OperationBehavior(ReleaseInstanceMode=ReleaseInstanceMode.AfterCall)]
		void ServerClosing();
    }
}
