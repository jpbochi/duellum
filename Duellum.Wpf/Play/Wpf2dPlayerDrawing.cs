﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Animation;
using Duellum.Wpf2d.Plugin.Drawing;

namespace Duellum.Wpf.Play
{
	public class Wpf2dPlayerDrawing : BasePlayerDrawing
	{
		public Wpf2dPlayer Player { get; private set; }

		public Wpf2dPlayerDrawing(Wpf2dPlayer player)
		{
			this.Player = player;
		}

		public override sealed Brush Brush			{ get { return new SolidColorBrush(this.Player.Color); } }
		public override sealed double Thickness		{ get { return 3; } }
		public double Rotation						{ get { return this.Player.FaceAngle * (180 / Math.PI); } }

		public override void Render(DrawingContext dc)
		{
			dc.PushTransform(new RotateTransform(Rotation));
			
			base.Render(dc);
			
			dc.Pop();
		}
	}

	//public class Wpf2dPlayerDrawing : BasePlayerDrawing
	//{
	//    //public Wpf2dPlayer Player { get; private set; }

	//    private Brush brush;
	//    private double thickness;

	//    public Wpf2dPlayerDrawing(Wpf2dPlayer player)
	//    {
	//        //Player = player
			
	//        brush = new SolidColorBrush(player.Color);
	//        thickness = 3;
	//    }

	//    public override sealed Brush Brush			{ get { return brush; } }
	//    public override sealed double Thickness		{ get { return thickness; } }
	//}
}
