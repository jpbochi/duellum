﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Duellum.Game;
using Duellum.Map;
using Duellum.Core;
using System.Windows.Media;
using Duellum.Wpf2d.Plugin;
using Duellum.Wpf2d.Plugin.Drawing;

namespace Duellum.Wpf.Play
{
	[DuelTypeXmlName("Wpf2dPlayerType")]
	public class Wpf2dPlayerType : DuelPlayerType
	{
		public const string DuelTypeId = "Wpf2d.Player";

		public Wpf2dPlayerType(DuelTypeId id) : base(id)
		{
		}

		public override BaseDuelObj CreateObj()
		{
			return new Wpf2dPlayer(this);
		}
	}

	public class Wpf2dPlayer : DuelPlayer
	{
		static public readonly AugProperty ColorProp =
			AugProperty.Create(
				"Color", typeof(Color), typeof(DuelPlayer), Colors.Black
			);

		public new DuelPlayerType Type { get { return (Wpf2dPlayerType)base.Type; } }

		public Wpf2dPlayer(Wpf2dPlayerType playerType) : base(playerType)
		{
			UpdateDrawing();
		}
		
		private void UpdateDrawing()
		{
			this.SetValue(Wpf2dPlugin.HexImageProp, Wpf2dPlugin.UseDrawing);
			this.SetValue(Wpf2dPlugin.DrawingProp, new Wpf2dPlayerDrawing(this));
		}

		public Color Color
		{
			get { return (Color)GetValue(ColorProp); }
			set { SetValue(ColorProp, value); UpdateDrawing(); }
		}
	}
}
