﻿using System.Windows;
using Duellum.Core;

namespace Duellum.Wpf
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public new static App Current { get { return Application.Current as App; } }

		public DuelDomain CurrentDomain { get; private set; }
		
		private void Application_Startup(object sender, StartupEventArgs e)
		{
			//TODO: make a plugin manager or, much better, use a good one that's supported by Microsoft like MEF!
			
			CurrentDomain = DuelDomain.Create();
			
			CurrentDomain.AttachPlugins(DuelPluginLoader.FindPlugins());
		}
	}
}
