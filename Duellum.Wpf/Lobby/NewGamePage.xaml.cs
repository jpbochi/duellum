﻿using System;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Duellum.Map;
using Duellum.Wpf.Main;
using System.Windows.Media;
using Duellum.Game;
using Duellum.Core;
using System.Collections.Generic;
using Duellum.Wpf.Play; //TODO: this reference is a code smell

namespace Duellum.Wpf.Lobby
{
	/// <summary>
	/// Interaction logic for NewGamePage.xaml
	/// </summary>
	public partial class NewGamePage : PageFunction<WizardResult>
	{
		private class GameParameters
		{
			public DuelMap Map { get; set; }

			public IEnumerable<DuelPlayer> Players { get; set; }
		}

		//private class PlayerParameters
		//{
		//    public string Name { get; set; }
		//    public Color Color { get; set; }
		//}

		private GameParameters newGameContext;
		
		public NewGamePage()
		{
			InitializeComponent();
			
			//this.RemoveFromJournal = true;
			
			newGameContext = new GameParameters();

			
			//newGameContext.Players = new []
			//{
			//    new Wpf2dPlayerType() { Name="Fau", Color=Colors.Red },
			//    new Wpf2dPlayerType() { Name="Jairo", Color=Colors.Blue }
			//};
			newGameContext.Players = new []
			{
			    CreatePlayer("Fau", Colors.Red),
			    CreatePlayer("Jairo", Colors.Blue),
			    CreatePlayer("Fábio", Colors.MediumOrchid),
			};
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			gridPlayers.ItemsSource = newGameContext.Players;
		}

		private Wpf2dPlayer CreatePlayer(string name, Color color)
		{
			var domain = App.Current.CurrentDomain;
		    var playerType = domain.GetType(Wpf2dPlayerType.DuelTypeId);
		    var player = (Wpf2dPlayer)playerType.CreateObj();
			
		    player.Name = name;
		    player.Color = color;

		    return player;
		}

		private void BrowseBack_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			base.OnReturn(new ReturnEventArgs<WizardResult>(WizardResult.Canceled));
			//e.Handled = true;
			
			//NavigationService.GetNavigationService(this).GoBack();
		}

		private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void Open_Executed(object sender, RoutedEventArgs e)
		{
			var dialog = DialogProvider.GetOpenMapDialog();
			
			bool? ok = dialog.ShowDialog();
			if (ok ?? false)
			{
				this.txtMapPath.Text = dialog.FileName;
				
				//TODO: handle exceptions when trying to open a map
				
				Stream fileStream = dialog.OpenFile();
				
				var domain = App.Current.CurrentDomain;
				var map = DuelMap.LoadMap(domain, new StreamReader(fileStream));
				
				UpdateMapDetails(map);
			}
		}
		
		private void UpdateMapDetails(DuelMap map)
		{
			string mapName;
			lblMapName.Content = map.Attributes.TryGetValue(DuelMapAttributes.Name, out mapName) ? mapName : "-";
			lblMapDimensions.Content = string.Format("{0}x{1}x{2}", map.DimX+1, map.DimY+1, map.DimZ+1);				
			lblMapCellType.Content = "Hexagonal";
			lblMapNotes.Content = "-";
			
			this.newGameContext.Map = map;
		}

		private void InitializeGame_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute
				= (newGameContext != null)
				&& (newGameContext.Map != null)
				&& (newGameContext.Players.Count() >= 2);
		}

		private void InitializeGame_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			//// Go to next Wizard page
			//WizardPage3 WizardPage3 = new WizardPage3((WizardData)this.DataContext);
			//WizardPage3.Return += new ReturnEventHandler<WizardResult>(WizardPage_Return);
			//this.NavigationService.Navigate(WizardPage3);
			
			//TODO: create a "game initialized" page
			
			var gamePage = new GamePage(new DuelGame(this.newGameContext.Map, this.newGameContext.Players));
			
			this.NavigationService.Navigate(gamePage);
		}
	}
}
