﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Duellum.Core;
using JpLabs.Extensions;

namespace Duellum.Wpf
{
	[DuelPlugin("WPF 2D Game Plugin")]
	public class GamePlugin : IDuelPlugin
	{
		private DuelPluginXmlConfigReader xmlConfig;
		private DuelDomain domain;
		
		DuelDomain IDuelPlugin.Domain
		{
			get { return domain; }
			set {
				if (domain != null) throw new InvalidOperationException("Domain already set");
				if (value == null) throw new ArgumentNullException("value");
				domain = value;
			}
		}
		
		void ISupportInitialize.BeginInit()
		{
			xmlConfig = new DuelPluginXmlConfigReader(
				domain,
				new Uri("pack://siteoforigin:,,,/config/gameplugin.xml", UriKind.Absolute),
				typeof(Wpf2d.Plugin.Wpf2dPlugin).Namespace
			);
		}
		
		void ISupportInitialize.EndInit()
		{
			xmlConfig.Dispose();
			xmlConfig = null;
		}

		IEnumerable<DuelPluginName> IDuelPlugin.GetReferencedPlugins()
		{
			return DuelPluginName.FromType(typeof(Wpf2d.Plugin.Wpf2dPlugin)).ToEnumerable();
		}

		IEnumerable<DuelType> IDuelPlugin.GetTypes()
		{
			return xmlConfig.GetTypes();
		}

		IEnumerable<DuelDecoration> IDuelPlugin.GetDecorations()
		{
			return xmlConfig.GetDecorations();
		}
		
		public void Dispose()
		{
			if (xmlConfig != null) xmlConfig.Dispose();
		}
	}
}
