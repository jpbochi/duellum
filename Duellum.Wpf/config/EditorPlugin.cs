﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Duellum.Core;
using JpLabs.Extensions;

namespace Duellum.Wpf
{
	[DuelPlugin("WPF 2D Map Editor Plugin")]
	public class EditorPlugin : IDuelPlugin
	{
		static public readonly AugProperty UIOrderProp =
			AugProperty.Create(
				"UIOrder", typeof(int), typeof(EditorPlugin), default(int)
			);

		private DuelPluginXmlConfigReader xmlConfig;
		private DuelDomain domain;
		
		DuelDomain IDuelPlugin.Domain
		{
			get { return domain; }
			set {
				if (domain != null) throw new InvalidOperationException("Domain already set");
				if (value == null) throw new ArgumentNullException("value");
				domain = value;
			}
		}
		
		void ISupportInitialize.BeginInit()
		{
			xmlConfig = new DuelPluginXmlConfigReader(
				domain,
				new Uri("pack://siteoforigin:,,,/config/editorplugin.xml", UriKind.Absolute),
				typeof(EditorPlugin).Namespace
			);
		}
		
		void ISupportInitialize.EndInit()
		{
			xmlConfig.Dispose();
			xmlConfig = null;
		}

		IEnumerable<DuelPluginName> IDuelPlugin.GetReferencedPlugins()
		{
			return DuelPluginName.FromType(typeof(CorePlugin)).ToEnumerable();
		}

		IEnumerable<DuelType> IDuelPlugin.GetTypes()
		{
			return xmlConfig.GetTypes();
		}

		IEnumerable<DuelDecoration> IDuelPlugin.GetDecorations()
		{
			return xmlConfig.GetDecorations();
		}
		
		public void Dispose()
		{
			if (xmlConfig != null) xmlConfig.Dispose();
		}
		
		static public int GetUIOrder(AugObject duelObject)
		{
			return duelObject.GetValue<int>(UIOrderProp);
		}
	}
}
