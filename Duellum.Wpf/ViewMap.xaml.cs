﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Duellum.Wpf
{
	/// <summary>
	/// Interaction logic for ShowMap.xaml
	/// </summary>
	public partial class ViewMap : Window
	{
		public ViewMap()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			//KeyBinding keybinding = new KeyBinding(NavigationCommands.IncreaseZoom, keygesture);
			//this.InputBindings.Add(keybinding); // I don't know why this doesn't work

			NavigationCommands.IncreaseZoom.InputGestures.Add(new KeyGesture(Key.Add));
			NavigationCommands.DecreaseZoom.InputGestures.Add(new KeyGesture(Key.Subtract));
			NavigationCommands.Zoom.InputGestures.Add(new KeyGesture(Key.Multiply));
		}
	}
}
