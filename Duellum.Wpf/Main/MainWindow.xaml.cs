﻿using System;
//using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Duellum.Map;
using Duellum.Wpf.Edit;

namespace Duellum.Wpf.Main
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			
			//this.AddHandler(CommandManager.ExecutedEvent, new ExecutedRoutedEventHandler(CmdExecuted));
			
			//object ugh = this.button2.GetValue(DockPanel.LastChildFillProperty);
			//this.button2.SetValue(DockPanel.LastChildFillProperty, false);
			//ugh = this.button2.GetValue(DockPanel.LastChildFillProperty);
			
			//GetValue()
			//DockPanel
			//CoerceValueCallback
			//dockMain.Background
			//System.Windows.Style
			//Setter
			//Trigger
			//Style
			//FrameworkPropertyMetadata
			//LogicalTreeHelper
			
			//WPF Commands - http://en.csharp-online.net/WPF_Concepts%E2%80%94Built-In_Commands
		}
		
		void OpenCmd_Executed(object target, ExecutedRoutedEventArgs e)
		{
			var mapFileStream = e.Parameter as FileStream;
			if (mapFileStream != null)
			{
				OpenMapForEdit(mapFileStream.Name, mapFileStream);
			}
			else
			{
				var dialog = DialogProvider.GetOpenMapDialog();
				
				bool? ok = dialog.ShowDialog();
				if (ok ?? false) OpenMapForEdit(dialog.SafeFileName, dialog.OpenFile());
			}
		}

		private void OpenMapForEdit(string fileName, Stream fileStream)
		{
			var domain = App.Current.CurrentDomain;
			
			var map = DuelMap.LoadMap(domain, new StreamReader(fileStream));
			
			var viewMap = new EditMapWindow()
			{
				Owner = this, //Window.GetWindow(this); //(Window)this
				Icon = this.Icon,
				WindowStartupLocation = WindowStartupLocation.CenterScreen,
			};
			viewMap.Title = string.Format(viewMap.Title, fileName);
			viewMap.mapViewer.Map = map;
			viewMap.Unloaded += ((sender, ev) => {this.Focus();});
			
			viewMap.Show();
		}

		void OpenCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = this.mainFrame.Content is MainMenu; //true;
		}

		void NewGameCmd_Executed(object target, ExecutedRoutedEventArgs e)
		{
			//TODO: Create a "New Game Options" window
			
			//var dialog = DialogProvider.GetOpenMapDialog();
			//bool? ok = dialog.ShowDialog();
			//if (ok ?? false) OpenMapForPlay(dialog.SafeFileName, dialog.OpenFile());
			OpenNewGamePage();
		}

		void NewGameCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = this.mainFrame.Content is MainMenu;
		}

		void ConnectToGameCmd_Executed(object target, ExecutedRoutedEventArgs e)
		{
			//TODO: implement this
		}

		void ConnectToCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = this.mainFrame.Content is MainMenu;
		}

		void OpenNewGamePage()
		{
			//TODO: Study Navigation Topologies Overview (http://msdn.microsoft.com/en-us/library/aa970446.aspx)
			
			//this.mainFrame.Content = new NewGamePage();
			this.mainFrame.Navigate(new Uri("/lobby/newgamepage.xaml", UriKind.RelativeOrAbsolute));
			//this.mainFrame.Navigate(new NewGamePage());
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			//this.tempBorder.Child = new Ma
			//this.mainFrame.Navigate(
		}

		//void OpenMapForPlay(string fileName, Stream fileStream)
		//{
		//    var map = DuelMap.LoadMap(fileStream);
		//    var gamePage = new NewGamePage();
		//    gamePage.mapViewer.Map = map;
		//    this.Content = gamePage;

		//    //var gameWindow = new Game();
		//    //gameWindow.Owner = this;
		//    //gameWindow.Icon = this.Icon;
		//    //gameWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
		//    //gameWindow.mapViewer.Map = map;
		//    //gameWindow.Show();
		//    //gameWindow.Unloaded += ((sender, ev) => {this.Focus();});
		//}
	}
}
