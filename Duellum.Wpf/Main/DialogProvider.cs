﻿using System.IO;
using Microsoft.Win32;

namespace Duellum.Wpf.Main
{
	internal class DialogProvider
	{
		private static OpenFileDialog pOpenMapDialog;
		
		static public OpenFileDialog GetOpenMapDialog()
		{
			if (pOpenMapDialog == null) {
				pOpenMapDialog = new OpenFileDialog();
				//pOpenMapDialog.InitialDirectory = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
				pOpenMapDialog.InitialDirectory = Directory.GetCurrentDirectory();
				pOpenMapDialog.Title = "Choose a Map";
				pOpenMapDialog.Multiselect = false;
				pOpenMapDialog.Filter = "Maps (*.mapx; *.map)|*.mapx;*.map|Any file (*.*)|*.*";
			}
			return pOpenMapDialog;
		}
	}
}
