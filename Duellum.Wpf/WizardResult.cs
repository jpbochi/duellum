﻿
namespace Duellum.Wpf
{
    public enum WizardResult
    {
        Finished,
        Canceled
    }
}
