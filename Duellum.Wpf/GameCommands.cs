﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using JpLabs.Extensions;

namespace Duellum.Wpf
{
	public static class GameCommands
	{
		// Fields
		private static RoutedCommand[] _internalCommands = new RoutedCommand[(int)CommandId.Last];

		// Methods
		private static RoutedCommand EnsureCommand(CommandId idCommand)
		{
			lock (_internalCommands.SyncRoot)
			{
				if (_internalCommands[(int) idCommand] == null)
				{
					string name = idCommand.ToString();
					
					//string text = idCommand.ToDescription();					
					//RoutedUICommand command = new RoutedUICommand(text, name, typeof(GameCommands));
					
					var command = new RoutedCommand(name, typeof(GameCommands));
					
					_internalCommands[(int) idCommand] = command;
				}
			}
			return _internalCommands[(int) idCommand];
		}

		private static string GetPropertyName(CommandId commandId)
		{
			return commandId.GetDescription();
		}

		// Properties
		public static RoutedCommand New { get { return EnsureCommand(CommandId.NewGame); } }

		public static RoutedCommand ConnectTo { get { return EnsureCommand(CommandId.ConnectToGame); } }

		public static RoutedCommand Initialize { get { return EnsureCommand(CommandId.InitializeGame); } }

		public static RoutedCommand Start { get { return EnsureCommand(CommandId.StartGame); } }

		// Nested Types
		private enum CommandId
		{
			NewGame,
			ConnectToGame,
			InitializeGame,
			StartGame,
			
			
			Last //Not a real command
		}
	}
}
