﻿using System.Collections.Generic;
using System.Linq;
using Duellum.Core;

namespace Duellum.Map
{
	public static class MapQueryExt
	{
		static public IEnumerable<DuelAbstraction> GetObjects(this IEnumerable<IDuelMapEntry> query)
		{
			return query.Select(entry => entry.Object);
		}

		static public IEnumerable<IDuelMapEntry> WhereIsGround(this IEnumerable<IDuelMapEntry> query)
		{
			return query.Where(entry => entry.Object.IsGround());
		}
		static public IEnumerable<IDuelMapEntry> WhereIsNotGround(this IEnumerable<IDuelMapEntry> query)
		{
			return query.Where(entry => !entry.Object.IsGround());
		}
	}
}
