﻿using System;
using System.Windows.Media;

namespace Duellum.Wpf2d.Plugin
{
    /// <summary>
    /// This class is used internally to delegate it's rendering to the parent OverlayRenderDecorator.
    /// </summary>
    public class OverlayVisual : DrawingVisual
    {
		public int ZOrder { get; set; }
        public bool IsHitTestVisible { get; set; }

        //don't hit-test, these are just overlay graphics
        protected override GeometryHitTestResult HitTestCore(GeometryHitTestParameters hitTestParameters)
        {
            if (IsHitTestVisible) return base.HitTestCore(hitTestParameters);
			return null;
        }

        //don't hit-test, these are just overlay graphics
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            if (IsHitTestVisible) return base.HitTestCore(hitTestParameters);
            return null;
        }
        
        public void Render(Action<DrawingContext> action)
        {
			using (var dc = this.RenderOpen()) action(dc);
        }

        public void Render(Action<DrawingContext> action, Transform transform)
        {
			using (var dc = this.RenderOpen())
			{
				dc.PushTransform(transform);
				action(dc);
				dc.Pop();
			}
        }
    }
}
