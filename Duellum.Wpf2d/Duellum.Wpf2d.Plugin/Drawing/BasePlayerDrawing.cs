﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Animation;

namespace Duellum.Wpf2d.Plugin.Drawing
{
	public abstract class BasePlayerDrawing : IDrawing2D
	{
		//public const double DefaultThickness = 2;
		//public const double DefaultRotationSpeed = 0;
		//static public readonly Color DefaultBackCursorColor = Color.FromArgb(0x7F, 0x7F, 0x7F, 0x7F);
		
		public abstract Brush Brush { get; }
		public abstract double Thickness { get; }

		private Pen pen;
		private Geometry geometry;

		Rect IDrawing2D.Bounds
		{
			get {
				var hexSize = Wpf2dPlugin.DefaultHexSize;
				
				return new Rect(
					-(hexSize.Width / 2), -(hexSize.Height / 2),
					hexSize.Width, hexSize.Height
				);
			}
		}

		public virtual void Render(DrawingContext dc)
		{
			if (this.geometry == null) PrepareGeometry();
			if (this.pen == null) PreparePen();
			
			dc.DrawGeometry(this.Brush, this.pen, this.geometry);
		}
		
		public virtual void InvalidateDrawing()
		{
			this.geometry = null;
			this.pen = null;
		}

		private void PrepareGeometry()
		{
			var hexSize = Wpf2dPlugin.DefaultHexSize;

			double width = 0.4 * hexSize.Width;
			double height = 0.6 * hexSize.Height;
			double halfWidth = width / 2;
			double halfHeight = height / 2;

			var pFront		= new Point(halfHeight, 0);
			var pBackLeft	= new Point(-halfHeight, halfWidth);
			var pBackRight	= new Point(-halfHeight, -halfWidth);

			var points = new PointCollection(new [] { pFront, pBackRight, pBackLeft });

			this.geometry = GetGeometry(points);
		}

		private Geometry GetGeometry(PointCollection points)
		{
			PathFigure figure = new PathFigure();
			if (points == null)
			{
				return Geometry.Empty;
			}
			else
			{
				if (points.Count > 0)
				{
					figure.StartPoint = points[0];
					if (points.Count > 1)
					{
						figure.Segments.Add(new PolyLineSegment(points.Skip(1), true));
					}
					figure.IsClosed = true;
					figure.IsFilled = false;
				}
				PathGeometry geometry = new PathGeometry();
				geometry.Figures.Add(figure);
				geometry.Freeze();
				
				return geometry;
			}
		}
		
		private void PreparePen()
		{
			this.pen = GetForegroundPen();
		}
		
		private Pen GetForegroundPen()
		{
			if (this.Brush == null) return null;
			
			var pen = new Pen(this.Brush, this.Thickness) {
				DashStyle = new DashStyle(new double[]{1, 1}, 0),
				DashCap = PenLineCap.Round,
				EndLineCap = PenLineCap.Round,
				LineJoin = PenLineJoin.Round
			};

			//{
			//    var rotateAnim = new DoubleAnimation() {
			//        From = 0,
			//        To = pen.DashStyle.Dashes.Sum(),
			//        Duration = new Duration(TimeSpan.FromSeconds(1)),
			//        RepeatBehavior = RepeatBehavior.Forever
			//    };
			//    pen.DashStyle.BeginAnimation(DashStyle.OffsetProperty, rotateAnim);
			//}
			
			return pen;
		}
	}
}
