﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Duellum.Core;
using JpLabs.Geometry;
using System.Windows;
using System.Windows.Media.Animation;
using Duellum.Wpf2d.Plugin.Drawing;

namespace Duellum.Wpf2d.Plugin
{
	[Obsolete("Use multiple MapCursorRenderer's instead")]
    public class MapMultiCursorRenderer : BaseMapMultipleRenderer
    {
		protected IList<MapCursor> Cursors { get; private set; }
		
		public MapMultiCursorRenderer()
		{
			Cursors = new List<MapCursor>();
		}

		//public IMouseClickable VisualHostAsClickable
		//{
		//    get { return VisualHost as IMouseClickable; }
		//}
		
		public void AddCursor(MapCursor cursor, int zOrder)
		{
			cursor.Visual = CreateVisual(zOrder);
			AddVisual(cursor.Visual);
			Cursors.Add(cursor);
		}
		
		public void RemoveCursor(MapCursor cursor)
		{
			RemoveVisual(cursor.Visual);
			Cursors.Remove(cursor);
		}
		
		public override void OnRender()
		{
			foreach (var c in Cursors) c.Render(c.Visual);
		}
	}
	
	[Obsolete]
	public class MapCursor
	{
		public IDrawing2D Drawing		{ get; set; }
		internal OverlayVisual Visual	{ get; set; }

		public MapCursor(IDrawing2D drawing)
		{
			if (drawing == null) throw new ArgumentNullException("drawing");
			
			this.Drawing = drawing;
			
			position.ValueChanged += Position_Changed;
		}

		private readonly TrackableValue<PointInt?> position = new TrackableValue<PointInt?>();
		public PointInt? Position
		{
			get { return position.Value; }
			set { position.Value = value; }
		}

		public event EventHandler<ValueChangedEventArgs<PointInt?>> PositionChanged
		{
			add		{ position.ValueChanged += value; }
			remove	{ position.ValueChanged -= value; }
		}
		
		private void Position_Changed(object sender, ValueChangedEventArgs<PointInt?> e)
		{
			Render(this.Visual);
		}

		internal void Render(OverlayVisual visual)
		{
			this.Drawing.Render(visual, this.Position);
		}
	}
}
