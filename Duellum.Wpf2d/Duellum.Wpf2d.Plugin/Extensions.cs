﻿using System;
using System.Windows;
using System.Windows.Media;
using DrawingColor = System.Drawing.Color;

namespace Duellum.Wpf2d.Plugin
{
	public static class ColorExt
	{
		static public Color ToColor(this DrawingColor drawingColor)
		{
			return Color.FromArgb(drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B);
		}
	}
}