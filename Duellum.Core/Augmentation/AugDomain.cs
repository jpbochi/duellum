﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Duellum.Core.ExceptionHelper;
using JpLabs.Extensions;

namespace Duellum.Core.Augmentation
{
	public sealed class AugDomain
	{
		static public readonly AugDomain Current;
		static AugDomain() { Current = new AugDomain(); }

		private readonly HashSet<string> assembliesScannedForProperties;
		private readonly HashSet<AugProperty> registeredProperties;
		
		private AugDomain()
		{
			assembliesScannedForProperties = new HashSet<string>();
			registeredProperties = new HashSet<AugProperty>();
			
			foreach (var asm in AppDomain.CurrentDomain.GetAssemblies()) RegisterAugPropertiesFromAssembly(asm);
			
			AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;
		}

		void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			RegisterAugPropertiesFromAssembly(args.LoadedAssembly);
		}

		private void RegisterProperty(AugProperty ap)
		{
			if (!registeredProperties.Add(ap)) throw Error.AugPropertyAlreadyRegistered(ap);
			
			cachedPropertiesByName = null;
		}
		
		ILookup<string,AugProperty> cachedPropertiesByName;
		internal ILookup<string,AugProperty> PropertiesByName
		{
			get {
				if (cachedPropertiesByName == null) cachedPropertiesByName = registeredProperties.ToLookup(p => p.Name);
				return cachedPropertiesByName;
			}
		}
		
		public AugProperty FindProp(string name, Type ownerType)
		{
			IEnumerable<AugProperty> q
				= PropertiesByName[name]
				.Where(ap => ap.OwnerType.IsAssignableFrom(ownerType))
				.TopologicalOrderBy(ap => ap.OwnerType, TypeComparer.Instance);
			
			//TODO: TEST property overriding
			//If a class "overrides" a property, the most specific should be returned
			//Considering that subclasses are smaller than base classes, I'll return the smallest			
			return q.FirstOrDefault();
		}

		public AugProperty FindProp(string name, string ownerName, params string[] namespaces)
		{
			IEnumerable<AugProperty> q
				= PropertiesByName[name]
				.Where(ap => ap.OwnerType.Name == ownerName);
			
			if (namespaces != null && namespaces.Length != 0) q = q.Where(ap => namespaces.Contains(ap.OwnerType.Namespace));
			
			return q.SingleOrDefault();
		}

		private void RegisterAugPropertiesFromAssembly(Assembly asm)
		{
			var companyAttr = asm.GetSingleAttrOrNull<AssemblyCompanyAttribute>(false);
			if (companyAttr != null && companyAttr.Company == "Microsoft Corporation") return; //ignore MS assemblies
			
			if (asm is System.Reflection.Emit.AssemblyBuilder) return; //ignore dynamic assemblies

			var clrFieldsWithAugProperties = DuelAssemblyParser.GetFieldsHoldingAugProperties(asm);
			
			var augProperties = clrFieldsWithAugProperties.Select( p => (AugProperty)p.GetValue(null) );
			
			foreach (var ap in augProperties)
			{
				this.RegisterProperty(ap);
			}
		}
	}
}
