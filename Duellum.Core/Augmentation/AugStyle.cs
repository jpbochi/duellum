﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Duellum.Core
{
	///System.Windows.Style
	///System.Windows.FrameworkElement.OnStyleChanged
	///System.Windows.StyleHelper.UpdateStyleCache
	/// http://www.interact-sw.co.uk/iangblog/2007/02/14/wpfdefaulttemplate

	/// WPF Styles = Setters + Triggers + (optional Template
	/// WPF Styles = Setters + Triggers + Visual Tree
	/// Duellum Styles = Setters + Initial Resources (Equipments and Skills)
	
	public class AugStyle
	{
		public AugStyle BasedOn { get; set; }

		//private object propValuesSyncRoot = new object();
		//private IDictionary<AugProperty,IAugValue> propValues; //dictionary is lazily created
		
		//public IEnumerable<KeyValuePair<AugProperty,IAugValue>> GetSetters()
		//{
		//    lock (propValuesSyncRoot) {
		//        return (
		//                (propValues != null)
		//                ? propValues.ToArray()
		//                : Enumerable.Empty<KeyValuePair<AugProperty,IAugValue>>()
		//            );
		//    }
		//}
		
		public ICollection<KeyValuePair<AugProperty,IAugValue>> Setters
		{
			get; private set;
		}
		
		//ISealable
		
		
	}
}
