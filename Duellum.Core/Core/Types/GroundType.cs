﻿using System.Collections.Generic;
using System.Linq;


namespace Duellum.Core.DuelCoreTypes
{
	public static class Ground
	{
		public const string Base				= "Ground.Base";
		
		public const string Regular				= "Ground.Regular";
		public const string Breaking			= "Ground.Breaking";
		
		static public IEnumerable<string> Enumerate() { return DuelCoreTypesExt.EnumerateType(typeof(Ground)); }
	}
}

namespace Duellum.Core
{
	public static class GroundExt
	{
		static public bool IsGround(this DuelAbstraction obj)
		{
			return obj.IsDescendantOf(DuelCoreTypes.Ground.Base);
		}

		static public IEnumerable<DuelAbstraction> WhereIsGround(this IEnumerable<DuelAbstraction> source)
		{
			return source.Where(o => o.IsGround());
		}
		static public IEnumerable<DuelAbstraction> WhereIsNotGround(this IEnumerable<DuelAbstraction> source)
		{
			return source.Where(o => !o.IsGround());
		}
	}
}

namespace Duellum.Core.Types
{
	[DuelTypeXmlName("GroundType")] //DuelCoreTypes.Ground.Base
	public class GroundType : DuelType
	{
		static public readonly DuelTypeId DefaultBaseTypeId = DuelCoreTypes.Ground.Base;

		public GroundType(DuelTypeId id) : base(id) { IsAbstract = false; }

		protected override DuelTypeId GetDefaultBaseType()
		{
			if (Id != DefaultBaseTypeId) return DefaultBaseTypeId;
			return base.GetDefaultBaseType();
		}

		public override BaseDuelObj CreateObj()
		{
			return new GroundObj(this);
		}
	}
	
	public class GroundObj : BaseDuelObj
	{
		public new GroundType Type { get { return (GroundType)base.Type; } }

		public GroundObj(GroundType type) : base(type) {}
	}
}
