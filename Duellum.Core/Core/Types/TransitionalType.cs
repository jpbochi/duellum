﻿using System.Collections.Generic;

namespace Duellum.Core.DuelCoreTypes
{
	public static class Transitional
	{
		public const string Base = "Transitional.Base";

		public const string Random_ActivatedLandMine = "Transitional.Random.ActivatedLandMine";
		public const string Random_BreakingGround	 = "Transitional.Random.BreakingGround";
		
		static public IEnumerable<string> Enumerate() { return DuelCoreTypesExt.EnumerateType(typeof(Transitional)); }
	}
}

namespace Duellum.Core.Types
{
	/// <summary>
	/// Transitional objects only exist in design-time, not in play-time
	/// </summary>
	[DuelTypeXmlName("TransitionalType")] //DuelCoreTypes.Transitional.Base
	public class TransitionalType : DuelType
	{
		static public readonly DuelTypeId DefaultBaseTypeId = DuelCoreTypes.Transitional.Base;

		public TransitionalType(DuelTypeId id) : base(id) { IsAbstract = false; }

		protected override DuelTypeId GetDefaultBaseType()
		{
			if (Id != DefaultBaseTypeId) return DefaultBaseTypeId;
			return base.GetDefaultBaseType();
		}

		public override BaseDuelObj CreateObj()
		{
			//return null;//throw new NotImplementedException();
			
			//ex.: a Random.ActivatedLandMine should be instantiated either as a activated landmine or nothing
			return new TransitionalObj(this);
		}
	}

	public class TransitionalObj : BaseDuelObj
	{
		public new TransitionalType Type { get { return (TransitionalType)base.Type; } }

		public TransitionalObj(TransitionalType type) : base(type) {}
	}	
}
