﻿using System.Collections.Generic;

namespace Duellum.Core.DuelCoreTypes
{
	public static class Wall
	{
		public const string Base	= "Wall.Base";

		public const string Wooden	= "Wall.Wooden";
		public const string Brick	= "Wall.Brick";
		public const string Stone	= "Wall.Stone";
		public const string Glass	= "Wall.Glass";
		public const string Fence	= "Wall.Fence";
		
		public const string SecretBrickDoor	= "Wall.SecretBrickDoor";
		public const string SteelDoor		= "Wall.SteelDoor";
		
		static public IEnumerable<string> Enumerate() { return DuelCoreTypesExt.EnumerateType(typeof(Wall)); }
	}
}

namespace Duellum.Core
{	
	public static class WallExt
	{
		static public bool IsWall(this DuelAbstraction obj)
		{
			return obj.IsDescendantOf(DuelCoreTypes.Wall.Base);
		}
	}
}

namespace Duellum.Core.Types
{
	//[Serializable]
	[DuelTypeXmlName("WallType")]
	public class WallType : DuelType
	{
		static public readonly AugProperty OpaquenessProp =
			AugProperty.Create(
				"Opaqueness", typeof(double), typeof(WallType), 0d
			);
		static public readonly AugProperty WalkableProp =
			AugProperty.Create(
				"Walkable", typeof(double), typeof(WallType), 0d
			);
		static public readonly AugProperty SolidnessProp =
			AugProperty.Create(
				"Solidness", typeof(double), typeof(WallType), 0d
			);

		//TODO: tranform this into an attached effect (or not)
		static public readonly AugProperty IsDoorProp =
			AugProperty.Create(
				"IsDoor", typeof(bool), typeof(WallType), false
			);

		static public readonly DuelTypeId DefaultBaseTypeId = DuelCoreTypes.Wall.Base;
		
		public WallType(DuelTypeId id) : base(id)
		{
			IsAbstract = false;
		}

		protected override DuelTypeId GetDefaultBaseType()
		{
			if (Id != DefaultBaseTypeId) return DefaultBaseTypeId;
			return base.GetDefaultBaseType();
		}

		public double Opaqueness
		{
			get { return (double)GetValue(OpaquenessProp); }
			set { SetValue(OpaquenessProp, value); }
		}
		public double Solidness
		{
			get { return (double)GetValue(SolidnessProp); }
			set { SetValue(SolidnessProp, value); }
		}
		public double Walkable
		{
			get { return (double)GetValue(WalkableProp); }
			set { SetValue(WalkableProp, value); }
		}

		public override BaseDuelObj CreateObj()
		{
			return new WallObj(this);
		}
	}
	
	public class WallObj : BaseDuelObj
	{
		public new WallType Type { get { return (WallType)base.Type; } }

		public WallObj(WallType type) : base(type) {}
	}
}
