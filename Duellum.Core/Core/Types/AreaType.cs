﻿using System.Collections.Generic;

namespace Duellum.Core.DuelCoreTypes
{
	public static class Area
	{
		public const string Base				= "Area.Base";

		public const string Fire				= "Area.Fire";
		public const string Smoke				= "Area.Smoke";
		public const string Teleport			= "Area.Teleport";
		public const string ElevatorDownOnly	= "Area.ElevatorDownOnly";
		public const string ElevatorUpDown		= "Area.ElevatorUpDown";
		public const string ElevatorUpOnly		= "Area.ElevatorUpOnly";
		
		static public IEnumerable<string> Enumerate() { return DuelCoreTypesExt.EnumerateType(typeof(Area)); }
	}
}

namespace Duellum.Core.Types
{
	[DuelTypeXmlName("AreaType")] //DuelCoreTypes.Area.Base
	public class AreaType : DuelType
	{
		static public readonly DuelTypeId DefaultBaseTypeId = DuelCoreTypes.Area.Base;

		public AreaType(DuelTypeId id) : base(id) { IsAbstract = false; }
		
		protected override DuelTypeId GetDefaultBaseType()
		{
			if (Id != DefaultBaseTypeId) return DefaultBaseTypeId;
			return base.GetDefaultBaseType();
		}
		
		public override BaseDuelObj CreateObj()
		{
			return new AreaObj(this);
		}
	}

	public class AreaObj : BaseDuelObj
	{
		public new AreaType Type { get { return (AreaType)base.Type; } }

		public AreaObj(AreaType type) : base(type) {}
	}
}
