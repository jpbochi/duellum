﻿using System.Collections.Generic;

namespace Duellum.Core.DuelCoreTypes
{
	public static class MapObj
	{
		public const string Base				= "MapObj.Base";
		
		public const string Bomb				= "MapObj.Bomb";
		public const string Corpse				= "MapObj.Corpse";
		public const string CovertVest			= "MapObj.CovertVest";
		public const string ExplosiveBarrel		= "MapObj.ExplosiveBarrel";
		public const string Grenade				= "MapObj.Grenade";
		public const string Grenade_Activated	= "MapObj.Grenade.Activated";
		public const string HolographicProjector= "MapObj.HolographicProjector";
		public const string InfraRedGoggles		= "MapObj.InfraRedGoggles";
		public const string InvisibilityHelmet	= "MapObj.InvisibilityHelmet";
		public const string Key					= "MapObj.Key";
		public const string Landmine			= "MapObj.Landmine";
		public const string LandMine_Activated	= "MapObj.LandMine.Activated";
		public const string MedicineKit			= "MapObj.MedicineKit";
		public const string Motorcycle			= "MapObj.Motorcycle";
		public const string Parasite			= "MapObj.Parasite";
		public const string ProximityDetector	= "MapObj.ProximityDetector";
		public const string RandomObjBox		= "MapObj.RandomObjBox";
		public const string RandomWeaponBox		= "MapObj.RandomWeaponBox";
		
		static public IEnumerable<string> Enumerate() { return DuelCoreTypesExt.EnumerateType(typeof(MapObj)); }
	}
}

namespace Duellum.Core.Types
{
	[DuelTypeXmlName("MapObj")]
	public class MapObjType : DuelType
	{
		static public readonly AugProperty LegacyCodeProp =
			AugProperty.Create(
				"LegacyCode", typeof(int?), typeof(MapObjType), null
			);

		static public readonly DuelTypeId DefaultBaseTypeId = DuelCoreTypes.MapObj.Base;

		public MapObjType(DuelTypeId id) : base(id) { IsAbstract = false; }

		protected override DuelTypeId GetDefaultBaseType()
		{
			if (Id != DefaultBaseTypeId) return DefaultBaseTypeId;
			return base.GetDefaultBaseType();
		}

		public int? LegacyCode
		{
			get { return (int?)GetValue(LegacyCodeProp); }
			set { SetValue(LegacyCodeProp, value); }
		}

		public override BaseDuelObj CreateObj()
		{
			return new MapObj(this);
		}
	}

	public class MapObj : BaseDuelObj
	{
		public new MapObjType Type { get { return (MapObjType)base.Type; } }

		public MapObj(MapObjType type) : base(type) {}
	}
}
