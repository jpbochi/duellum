﻿
namespace Duellum.Core.ExceptionHelper
{
	internal static class Strings
	{
		// Methods
		internal static string ArgumentArrayHasTooManyElements(object p0)
		{
			return SR.GetString("ArgumentArrayHasTooManyElements", p0);
		}

		internal static string ArgumentNotIEnumerableGeneric(object p0)
		{
			return SR.GetString("ArgumentNotIEnumerableGeneric", new object[] { p0 });
		}

		internal static string ArgumentNotLambda(object p0)
		{
			return SR.GetString("ArgumentNotLambda", new object[] { p0 });
		}

		internal static string ArgumentNotSequence(object p0)
		{
			return SR.GetString("ArgumentNotSequence", new object[] { p0 });
		}

		internal static string ArgumentNotValid(object p0)
		{
			return SR.GetString("ArgumentNotValid", new object[] { p0 });
		}

		internal static string NoArgumentMatchingMethodsInQueryable(object p0)
		{
			return SR.GetString("NoArgumentMatchingMethodsInQueryable", new object[] { p0 });
		}

		internal static string NoMethodOnType(object p0, object p1)
		{
			return SR.GetString("NoMethodOnType", new object[] { p0, p1 });
		}

		internal static string NoMethodOnTypeMatchingArguments(object p0, object p1)
		{
			return SR.GetString("NoMethodOnTypeMatchingArguments", new object[] { p0, p1 });
		}

		internal static string NoNameMatchingMethodsInQueryable(object p0)
		{
			return SR.GetString("NoNameMatchingMethodsInQueryable", new object[] { p0 });
		}

		// Properties
		internal static string EmptyEnumerable
		{
			get { return SR.GetString("EmptyEnumerable"); }
		}

		internal static string IncompatibleElementTypes
		{
			get { return SR.GetString("IncompatibleElementTypes"); }
		}

		internal static string MoreThanOneElement
		{
			get { return SR.GetString("MoreThanOneElement"); }
		}

		internal static string MoreThanOneMatch
		{
			get { return SR.GetString("MoreThanOneMatch"); }
		}

		internal static string NoElements
		{
			get { return SR.GetString("NoElements"); }
		}

		internal static string NoMatch
		{
			get { return SR.GetString("NoMatch"); }
		}

		internal static string OwningTeam
		{
			get { return SR.GetString("OwningTeam"); }
		}
	}
}
