﻿using System;

namespace Duellum.Core.ExceptionHelper
{
	internal static partial class Error
	{
		static public Exception ArgumentOfIncorrectType(Type expectedType, object value, string argumentName)
		{
			System.Diagnostics.Debugger.Break();
			return new ArgumentException(
				string.Format("'{0}' has to be assinable to {1}", (value == null) ? "{null}" : value, expectedType),
				argumentName
			);
		}

		static public Exception AugPropertyAlreadyRegistered(AugProperty ap)
		{
			System.Diagnostics.Debugger.Break();
			return new InvalidOperationException(string.Format("A augmented property named '{0}' was already registered to type '{1}'", ap.Name, ap.OwnerType.FullName));
		}

		static public Exception DuelTypeIsNotRegistered(DuelTypeId typeid)
		{
			return new InvalidOperationException(string.Format("The DuelType '{0}' is not registered", typeid.ToString()));
		}

		static public Exception TypeIsNotADuelPlugin(Type type)
		{
			return new InvalidOperationException(string.Format("Type '{0}' is not a Duellum Plugin", type.Name));
		}

		static public Exception DuelPluginNotAttached(DuelPluginName name)
		{
			return new InvalidOperationException(string.Format("Plugin '{0}' was not attached to DuelDomain", name.ToString()));
		}

		static public Exception DuelPluginAlreadyAttached(DuelPluginName name)
		{
			return new InvalidOperationException(string.Format("Plugin '{0}' was already attached to DuelDomain", name.ToString()));
		}

		static public Exception DuelPropertyNotFound(string propertyName)
		{
			return new InvalidOperationException(string.Format("Duel Property '{0}' not found", propertyName));
		}

		static public Exception DuelTypeNotFound(DuelTypeId typeId)
		{
			return new InvalidOperationException(string.Format("DuelType '{0}' not found. Maybe a plugin wasn't loaded", typeId));
		}
	}
}
