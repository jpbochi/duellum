﻿using System;

namespace Duellum.Core.ExceptionHelper
{
	internal static partial class Error
	{
		// Methods
		static public Exception ArgumentArrayHasTooManyElements(object p0)
		{
			return new ArgumentException(Strings.ArgumentArrayHasTooManyElements(p0));
		}

		static public Exception ArgumentNotIEnumerableGeneric(object p0)
		{
			return new ArgumentException(Strings.ArgumentNotIEnumerableGeneric(p0));
		}

		static public Exception ArgumentNotLambda(object p0)
		{
			return new ArgumentException(Strings.ArgumentNotLambda(p0));
		}

		static public Exception ArgumentNotSequence(object p0)
		{
			return new ArgumentException(Strings.ArgumentNotSequence(p0));
		}

		static public Exception ArgumentNotValid(object p0)
		{
			return new ArgumentException(Strings.ArgumentNotValid(p0));
		}

		static public Exception ArgumentNull(string paramName)
		{
			return new ArgumentNullException(paramName);
		}

		static public Exception ArgumentOutOfRange(string paramName)
		{
			return new ArgumentOutOfRangeException(paramName);
		}

		static public Exception IncompatibleElementTypes()
		{
			return new ArgumentException(Strings.IncompatibleElementTypes);
		}

		static public Exception MoreThanOneElement()
		{
			return new InvalidOperationException(Strings.MoreThanOneElement);
		}

		static public Exception MoreThanOneMatch()
		{
			return new InvalidOperationException(Strings.MoreThanOneMatch);
		}

		static public Exception NoArgumentMatchingMethodsInQueryable(object p0)
		{
			return new InvalidOperationException(Strings.NoArgumentMatchingMethodsInQueryable(p0));
		}

		static public Exception NoElements()
		{
			return new InvalidOperationException(Strings.NoElements);
		}

		static public Exception NoMatch()
		{
			return new InvalidOperationException(Strings.NoMatch);
		}

		static public Exception NoMethodOnType(object p0, object p1)
		{
			return new InvalidOperationException(Strings.NoMethodOnType(p0, p1));
		}

		static public Exception NoMethodOnTypeMatchingArguments(object p0, object p1)
		{
			return new InvalidOperationException(Strings.NoMethodOnTypeMatchingArguments(p0, p1));
		}

		static public Exception NoNameMatchingMethodsInQueryable(object p0)
		{
			return new InvalidOperationException(Strings.NoNameMatchingMethodsInQueryable(p0));
		}

		static public Exception NotImplemented()
		{
			return new NotImplementedException();
		}

		static public Exception NotSupported()
		{
			return new NotSupportedException();
		}
	}
}
