﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace Duellum.Core
{
	internal class DuelPluginProxy : IDuelPlugin
	{
		public DuelPluginName Name		{ get; private set; }
		internal IDuelPlugin Instance	{ get; private set; }
		
		private DuelPluginProxy(DuelPluginName name, IDuelPlugin instance)
		{
			Name = name;
			Instance = instance;
		}

		DuelDomain IDuelPlugin.Domain
		{
			get { return Instance.Domain; }
			set { Instance.Domain = value; }
		}

		public void BeginInit()
		{
			Instance.BeginInit();
		}

		internal static DuelPluginProxy Create(DuelPluginName name, IDuelPlugin instance)
		{
			return new DuelPluginProxy(name, instance);
		}

		public IEnumerable<DuelPluginName> GetReferencedPlugins()
		{
		    return Instance.GetReferencedPlugins() ?? Enumerable.Empty<DuelPluginName>();
		}

		public IEnumerable<DuelType> GetTypes()
		{
		    return Instance.GetTypes() ?? Enumerable.Empty<DuelType>();
		}

		//public IEnumerable<DuelDecoration> GetExtendedTypes()
		//{
		//    return Instance.GetExtendedTypes() ?? Enumerable.Empty<DuelDecoration>();
		//}

		public IEnumerable<DuelDecoration> GetDecorations()
		{
		    return Instance.GetDecorations() ?? Enumerable.Empty<DuelDecoration>();
		}
		
		public void EndInit()
		{
			Instance.EndInit();
		}

		public void Dispose()
		{
			Instance.Dispose();
		}
	}
}
